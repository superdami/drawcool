//
//  DrawView.h
//  DrawCool
//
//  Created by RakuTech on 10-8-24.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "Center.h"

#import "TouchView.h"
#import "HalfHideView.h"
#import "ToolView.h"

@protocol DrawViewDelegate <NSObject>
@required
- (void)hideDrawControlSubview;

@end

@interface DrawView : UIView <TouchViewDelegate, HHViewDelegate, UIGestureRecognizerDelegate>{
	
	Center *center;

	NSInteger currentFrameNum;	
	NSMutableArray *framesArray;
    NSMutableArray *linesArray;
	
	id<DrawViewDelegate> delegate;

	
	TouchView *touchView;
    HalfHideView *topControlView;
    ToolView *bottomControlView;
    
    GLuint unchangedTex;
    
    Frame *bgFrame;
    
    BOOL startPaint, paintMoved;
    BOOL isPreview;    
}
@property (nonatomic, retain)NSMutableArray *framesArray;
@property (nonatomic, assign)IBOutlet id<DrawViewDelegate> delegate;
@property (nonatomic, retain)IBOutlet TouchView *touchView;
@property (nonatomic, retain)IBOutlet HalfHideView *topControlView;
@property (nonatomic, retain)IBOutlet ToolView *bottomControlView;

@property (nonatomic, retain)Center *center;

@property (nonatomic, assign)NSInteger currentFrameNum;

- (IBAction)undo;
- (IBAction)switchWorkMode;

- (void)forceStopPlay;
- (void)loadAndRendFrame:(NSInteger)num; 
- (void)reflashAndRend;
- (void)startNewWork;
- (void)saveWork;
- (void)loadWork;
- (void)cleanAllObj;
@end
