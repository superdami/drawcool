//
//  DrawView.m
//  DrawCool
//
//  Created by RakuTech on 10-8-24.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DrawView.h"


@implementation DrawView

@synthesize framesArray;
@synthesize delegate;
@synthesize touchView;
@synthesize topControlView;
@synthesize bottomControlView;

@synthesize center;

@synthesize currentFrameNum;


+ (Class) layerClass
{
	return [CAEAGLLayer class];
}

- (id)initWithCoder:(NSCoder*)coder {
    
    if ((self = [super initWithCoder:coder])) {		
        
//        [self insertSubview:topControlView.scroller atIndex:1];
//        [topControlView.scroller release];

		framesArray = [[NSMutableArray array] retain];
        linesArray = [[NSMutableArray array] retain];
    
        bgFrame = [[Frame alloc] init];
        Transform a = {0.0, 0.0, 0.5, 0.5, 0.0};
        bgFrame.transform = a;
	}
	
	return self;
}

-(void)layoutSubviews
{
	[EAGLContext setCurrentContext:center.context];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [linesArray release];
	[center release];
    
    [framesArray release];
    [bgFrame release];
    
    [touchView release];
    [topControlView release];
    [bottomControlView release];
    
    [super dealloc];
}

- (void)cleanAllObj {
    
	[center destroyFramebuffer];
    [center destroyBrushTexture];
	currentFrameNum = 0;
}

- (BOOL)checkFrameSaved:(Frame *)f {
	if([center checkFileExists:f.texLabel Type:@"dat"]){
		return YES;
	}
	return NO;	
}

- (Frame *)getFrameAtNum:(NSInteger)num {
	if([framesArray count] >= num && num >= 1){
		Frame *f = [framesArray objectAtIndex:num - 1];
        return f;
	}
	else {
		return nil;
	}
}

- (void)saveCurrentFrame {
    [center saveTex:[center currentTex] forFrame:[self getFrameAtNum:currentFrameNum]];
}

- (IBAction)undo {
    NSInteger num = [linesArray count];
    if(num){
        Frame *currentFrame = [self getFrameAtNum:currentFrameNum];
        
        [center cleanColor:Color4Make(0.0, 0.0, 0.0, 0.0) TextureContext:[center currentTex]];
        [center bufferBlindTex:[center currentTex]];
        [center renderToBuffer:[center unvisibleFrameBuffer] Texture:unchangedTex Frame:nil];
        
        [linesArray removeLastObject];
        num = num - 1;
        
        for(int i = 0; i < num; i++){
            NSArray *line = [linesArray objectAtIndex:i];
            NSInteger n = [line count];
            
            NSArray *lineInfo = [line objectAtIndex:0];
            
            Color4 color;
            color.r = [[lineInfo objectAtIndex:0] floatValue];
            color.g = [[lineInfo objectAtIndex:1] floatValue];
            color.b = [[lineInfo objectAtIndex:2] floatValue];
            color.a = [[lineInfo objectAtIndex:3] floatValue];
            
            Frame *tempPen = [[[Frame alloc] init] autorelease];
            tempPen.color = color;
            tempPen.size = [[lineInfo objectAtIndex:4] floatValue];
            tempPen.tex = [[lineInfo objectAtIndex:5] floatValue];
            
            for(int j = 1; j < n; j++){
                NSArray *ptp = [line objectAtIndex:j];
                CGPoint a = CGPointFromString([ptp objectAtIndex:0]);
                CGPoint b = CGPointFromString([ptp objectAtIndex:1]);
                
                [center renderLineToBuffer:[center unvisibleFrameBuffer] Brush:tempPen FromPoint:a toPoint:b];
            }
        }
        [center renderFrame:currentFrame PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
    }
}

- (IBAction)switchWorkMode{
    if(isPreview){
        isPreview = NO;
        [center setTex:[center previousTex] WithLabel:[self getFrameAtNum:currentFrameNum - 1].texLabel];
        [center setTex:unchangedTex WithTex:[center currentTex]];
        [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
    }
    else {
        isPreview = YES;
        
        [self saveWork];
        [linesArray removeAllObjects];
        
        [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:nil AndBG:bgFrame To:[center visibleFrameBuffer]];
    }
    [delegate hideDrawControlSubview];
    [topControlView switchMode:isPreview];
    [bottomControlView switchMode:isPreview];
}

- (void)loadAndRendFrame:(NSInteger)num {
    currentFrameNum = num;
    
    [center setTex:[center previousTex] WithLabel:[self getFrameAtNum:currentFrameNum - 1].texLabel];
    [center setTex:[center currentTex] WithLabel:[self getFrameAtNum:currentFrameNum].texLabel];
    [center setTex:unchangedTex WithTex:[center currentTex]];
    
    [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
}

- (void)reflashAndRend {
    [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];    
}

- (void)saveWork {	
    [self saveCurrentFrame];
	[center saveParaFor:framesArray];
}

- (void)startNewWork {
	[EAGLContext setCurrentContext:center.context];
	
	[center destroyFramebuffer];
	[center createFramebuffer:self];
    [center createBrushTexture];
	
	[center initBackgroundWithColor:Color4Make(0.1, 0.5, 0.6, 1.0)];
	[center saveBackground];
	currentFrameNum = 0;
	
    [center setDrawBrush:0];
    
    unchangedTex = [center initNewTex];
    
    [self willAddNewFrame];
    [self didAddNewFrame];
    [topControlView resetCellNum:1];
}

- (void)loadWork {
	[EAGLContext setCurrentContext:center.context];
	
	[center destroyFramebuffer];
	[center createFramebuffer:self];
    [center createBrushTexture];
    
	[center initBackgroundWithInfo];
	[center loadParaFor:framesArray];
	
    unchangedTex = [center initNewTex];
	[self loadAndRendFrame:1];
    [topControlView resetCellNum:1];
    
    [center setDrawBrush:0];
}

#pragma mark TouchView Delegate Handler
- (BOOL)isPreviewMode {
    return isPreview;
}

- (void)transformOrigin:(CGPoint)origin Translation:(CGPoint)moveDistance Angle:(GLfloat)angle Scaling:(GLfloat)scale {
    Frame *currentFrame = [self getFrameAtNum:currentFrameNum];
    
    Transform t = currentFrame.transform;
    SetTransform(&t, origin, moveDistance, angle, scale);
    
    currentFrame.transform = t;
    
    [center renderFrame:currentFrame PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
    
    
    startPaint = YES;
    paintMoved = NO;
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [[event touchesForView:touchView] anyObject];
	CGPoint previousLocation = [touch previousLocationInView:touchView];
	previousLocation.y = self.bounds.size.height - previousLocation.y;
	CGPoint location = [touch locationInView:touchView];
	location.y = self.bounds.size.height - location.y;
    
    Frame *currentFrame = [self getFrameAtNum:currentFrameNum];
    center.brush.transform = currentFrame.transform;
    
    [center bufferBlindTex:[center currentTex]];
    [center renderLineToBuffer:[center unvisibleFrameBuffer] Brush:[center brush] FromPoint:previousLocation toPoint:location];
    [center renderFrame:currentFrame PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
    
    RevertTranslate(&previousLocation, currentFrame.transform);
    RevertTranslate(&location, currentFrame.transform);
    
    if(startPaint){
        startPaint = NO;
        
        NSArray *lineValue = [center returnBrushValues];
        NSArray *ptp = [NSArray arrayWithObjects:NSStringFromCGPoint(previousLocation), NSStringFromCGPoint(location), nil];
        NSMutableArray *line = [NSMutableArray arrayWithObjects:lineValue, ptp, nil];
        [linesArray addObject:line];
    }
    else {
        NSArray *ptp = [NSArray arrayWithObjects:NSStringFromCGPoint(previousLocation), NSStringFromCGPoint(location), nil];
        NSMutableArray *line = [linesArray lastObject];
        [line addObject:ptp];
    }
    paintMoved = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!paintMoved){
        UITouch *touch = [[event touchesForView:touchView] anyObject];
        CGPoint previousLocation = [touch previousLocationInView:touchView];
        previousLocation.y = self.bounds.size.height - previousLocation.y;
        CGPoint location = [touch locationInView:touchView];
        location.y = self.bounds.size.height - location.y;
        
        Frame *currentFrame = [self getFrameAtNum:currentFrameNum];
        center.brush.transform = currentFrame.transform;
        
        [center bufferBlindTex:[center currentTex]];
        [center renderLineToBuffer:[center unvisibleFrameBuffer] Brush:[center brush] FromPoint:previousLocation toPoint:location];
        [center renderFrame:currentFrame PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
        
        RevertTranslate(&previousLocation, currentFrame.transform);
        RevertTranslate(&location, currentFrame.transform);
        
        NSArray *lineValue = [center returnBrushValues];
        NSArray *ptp = [NSArray arrayWithObjects:NSStringFromCGPoint(previousLocation), NSStringFromCGPoint(location), nil];
        NSMutableArray *line = [NSMutableArray arrayWithObjects:lineValue, ptp, nil];
        [linesArray addObject:line];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

#pragma mark -
#pragma mark HHViewDelegate

- (NSInteger)currentFrameNum {
    return currentFrameNum;
}

- (NSInteger)frameCount {
    return [framesArray count];
}

- (void)didJumpToFrame:(NSInteger)num {
    if(isPreview){
        currentFrameNum = num;
        [center setTex:[center currentTex] WithLabel:[self getFrameAtNum:currentFrameNum].texLabel];
        [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:nil AndBG:bgFrame To:[center visibleFrameBuffer]];        
    }
    else {
        [self saveCurrentFrame];
        [linesArray removeAllObjects];
        [self loadAndRendFrame:num];       
    }
}

- (void)willAddNewFrame {
    Frame *newFrame = [[Frame alloc] init];
	
	[framesArray insertObject:newFrame atIndex:currentFrameNum];
    [newFrame release];
}

- (void)didAddNewFrame {
    
    if(currentFrameNum > 0)
    {
        [self saveCurrentFrame];
        [linesArray removeAllObjects];        
    }
    
    currentFrameNum++;

    [center setTex:[center previousTex] WithLabel:[self getFrameAtNum:currentFrameNum - 1].texLabel];
    [center cleanColor:Color4Make(0.0, 0.0, 0.0, 0.0) TextureContext:[center currentTex]];
    [center setTex:unchangedTex WithTex:[center currentTex]];
    
    [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];

}

- (void)willCopyAddFrame {
    Frame *newFrame = [[Frame alloc] init];
    newFrame.transform = [self getFrameAtNum:currentFrameNum].transform;
    
    [framesArray insertObject:newFrame atIndex:currentFrameNum - 1];
    [newFrame release];
}

- (void)didCopyAddFrame {
    [self saveCurrentFrame];
    [linesArray removeAllObjects];
    
    currentFrameNum++;
    [center setTex:[center previousTex] WithTex:[center currentTex]];
    [center setTex:unchangedTex WithTex:[center currentTex]];
    
    [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
}

BOOL needClean = NO;
- (void)willRemoveFrame {
    if([framesArray count] > 1){
        needClean = NO;
        [framesArray removeObjectAtIndex:currentFrameNum - 1];    
    }
    else {
        needClean = YES;
    }
}

- (void)didRemoveFrame {
    [linesArray removeAllObjects];
    
    if(currentFrameNum > 1){
        currentFrameNum--;    
        [center setTex:[center previousTex] WithLabel:[self getFrameAtNum:currentFrameNum - 1].texLabel];
        [center setTex:[center currentTex] WithLabel:[self getFrameAtNum:currentFrameNum].texLabel];
    }
    else {
        if(needClean){
            [center cleanColor:Color4Make(0.0, 0.0, 0.0, 0.0) TextureContext:[center previousTex]];
            [center cleanColor:Color4Make(0.0, 0.0, 0.0, 0.0) TextureContext:[center currentTex]];            
        }
        else {
            [center setTex:[center previousTex] WithLabel:[self getFrameAtNum:currentFrameNum - 1].texLabel];
            [center setTex:[center currentTex] WithLabel:[self getFrameAtNum:currentFrameNum].texLabel];    
        }
    }
    
    [center setTex:unchangedTex WithTex:[center currentTex]];
    [center renderFrame:[self getFrameAtNum:currentFrameNum] PervFrame:[self getFrameAtNum:currentFrameNum - 1] AndBG:bgFrame To:[center visibleFrameBuffer]];
}

- (void)forceStopPlay {
    [topControlView stopPlay];
}
@end
