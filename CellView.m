//
//  CellView.m
//  DrawCool
//
//  Created by RakuTech on 11-5-25.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "CellView.h"


@implementation CellView
@synthesize label;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"film"]];
        [self addSubview:background];
        [background release];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:label];
        [label release];
        [label setTextAlignment:UITextAlignmentCenter];
        [label setTextColor:[UIColor blackColor]];
        [label setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [label release];
    [super dealloc];
}

- (void)setHighLight 
{
//    [self setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor whiteColor]];
    [label setTextColor:[UIColor yellowColor]];
    [self setTransform:CGAffineTransformMakeScale(1.5, 1.5)];
}

- (void)setHighLightScale:(CGFloat)n 
{
    //    [self setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundColor:[UIColor whiteColor]];
    [label setTextColor:[UIColor blackColor]];
    [self setTransform:CGAffineTransformMakeScale(n, n)];
}

- (void)setNormal
{
    [self setBackgroundColor:[UIColor whiteColor]];
    [label setTextColor:[UIColor lightGrayColor]];
    [self setTransform:CGAffineTransformMakeScale(1.0, 1.0)];

}
@end
