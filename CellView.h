//
//  CellView.h
//  DrawCool
//
//  Created by RakuTech on 11-5-25.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CellView : UIView {
    UILabel *label;
    UIImageView *background;
}

@property (nonatomic, retain)UILabel *label;

- (void)setHighLight; 
- (void)setHighLightScale:(CGFloat)n;
- (void)setNormal;
@end

