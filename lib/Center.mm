//
//  Center.m
//  DrawCool
//
//  Created by RakuTech on 11-2-12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Center.h"

#define USE_DEPTH_BUFFER	0
#define kBrushPixelStep		0.1

#define PaperSizeWidth      640.0
#define PaperSizeHeight     960.0

#define ColorOne 0.0f, 0.0f, 0.0f, 0.0f

@implementation Center
@synthesize context;
@synthesize visibleFrameBuffer;	
@synthesize unvisibleFrameBuffer;
@synthesize currentTex, previousTex;

@synthesize currentWorkNum;
@synthesize worksArray;
  
@synthesize brush; 
//@synthesize background;
@synthesize isShowPreviousFrame;

const GLfloat coordsOne[] = {
	0.0, 1.0,
	1.0, 1.0,
	0.0, 0.0,
	1.0, 0.0       
};

const GLfloat coordsTwo[] = {
	0.0, 0.0,
	1.0, 0.0,
	0.0, 1.0,
	1.0, 1.0,
};

const GLfloat coordsThree[] = {
	0.0, 1.0,
	0.0, 0.0,
	1.0, 1.0,
	1.0, 0.0,
};

const GLfloat vertices[] = {
	0.0, PaperSizeHeight,
	PaperSizeWidth, PaperSizeHeight,
	0.0, 0.0,
	PaperSizeWidth, 0.0
};
const Color4 whiteColor = Color4Make(1.0, 1.0, 1.0, 1.0);
const Color4 transparentColor = Color4Make(1.0, 1.0, 1.0, 0.1);
const Transform originTrans = {0.0, 0.0, 1.0, 1.0, 0.0};
const Transform bgTrans = {0.0, 0.0, 0.75, 0.5, 0.0};

- (id) init {
	if ((self = [super init])) {
		context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
		if (!context || ![EAGLContext setCurrentContext:context]) {
			[self release];
			return NO;
		}
		
		worksArray = [[NSMutableArray array] retain];
		
//		background = [[Frame alloc] init];
//        background.transform = bgTrans;
        
        brush = [[Frame alloc] init];
        isShowPreviousFrame = YES;
	}	
	return self;	
}

- (void)dealloc {
	[context release];
	[worksArray release];
//    [background release];
    [brush release];
	[super dealloc];
}

- (void)renderTex:(GLuint)tex Transform:(Transform)transform Color:(Color4)color
{
    if(tex){
        glBindTexture(GL_TEXTURE_2D, tex);
        glColor4f(color.r, color.g, color.b, color.a);
        glPushMatrix();		
        
        glTranslatef(transform.translate.x, transform.translate.y, 0.0f);
        glRotatef(-transform.angle*180.0/M_PI, 0, 0, 1);
        glScalef(transform.scale.x, transform.scale.y, 1.0);
        
        glTexCoordPointer(2, GL_FLOAT, 0, coordsOne);
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glPopMatrix();    
    }	
}

- (void)renderBGTex:(GLuint)tex Transform:(Transform)transform Color:(Color4)color
{
    if(tex){
        glBindTexture(GL_TEXTURE_2D, tex);
        glColor4f(color.r, color.g, color.b, color.a);
        glPushMatrix();		
        
        glTranslatef(transform.translate.x, transform.translate.y, 0.0f);
        glRotatef(-transform.angle*180.0/M_PI, 0, 0, 1);
        glScalef(transform.scale.x, transform.scale.y, 1.0);
        
        glTexCoordPointer(2, GL_FLOAT, 0, coordsThree);
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glPopMatrix();    
    }	
}


- (GLubyte *)readPixFromBuffer:(GLuint)frameBuffer Blind:(GLuint)tex {
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, frameBuffer);  
	
	glClearColor(ColorOne);
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    [self renderTex:tex Transform:originTrans Color:whiteColor];
	glDisable(GL_BLEND);
	
	NSInteger myDataLength = PaperSizeWidth * PaperSizeHeight * 4;     
	GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, PaperSizeWidth, PaperSizeHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);	
	return buffer;
}

#pragma mark -
#pragma mark Render Fuction
- (BOOL)createFramebuffer:(UIView *)view
{
	CAEAGLLayer *eaglLayer = (CAEAGLLayer *)view.layer;
	eaglLayer.opaque = YES;
	eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
									[NSNumber numberWithBool:YES], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
	
	// Generate IDs for a framebuffer object and a color renderbuffer
	glGenFramebuffersOES(1, &visibleFrameBuffer);
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, visibleFrameBuffer);
	glGenRenderbuffersOES(1, &visibleRenderBuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, visibleRenderBuffer);
	[context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)view.layer];
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, visibleRenderBuffer);
	
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
	glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_RGBA8_OES, backingWidth, backingHeight);
	if(USE_DEPTH_BUFFER){
		glGenRenderbuffersOES(1, &depthRenderbuffer);
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
		glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
		glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
	}
	if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
	{
		NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
		return NO;
	}
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, 0);
    
    glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	glOrthof(0, PaperSizeWidth, 0, PaperSizeHeight, 0.0f, 1.0f);
	glViewport(0, 0, PaperSizeWidth, PaperSizeHeight);
    
    
    currentTex = [self initNewTex];
    previousTex = [self initNewTex];
    backgroundTex = [self initNewTex];
	return YES;
}

- (void)createBrushTexture {
    brushTex[0] = [self initNewTex];
    [self setTex:brushTex[0] WithImage:[UIImage imageNamed:@"Particle.png"]];
    brushTex[1] = 0;
    brushTex[2] = 0;
    brush.tex = brushTex[0];
    
    Color4 color = {0.4, 0.0, 0.3, 1.0};
    brush.color = color;
    brush.size = 10.0;
    brush.tex = 0;
}

- (void)destroyFramebuffer
{
	glDeleteFramebuffersOES(1, &visibleFrameBuffer);
	visibleFrameBuffer = 0;
	glDeleteRenderbuffersOES(1, &visibleRenderBuffer);
	visibleRenderBuffer = 0;
	
	if(depthRenderbuffer)
	{
		glDeleteRenderbuffersOES(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}
    
//    [self cleanTexFor:background];
    [self cleanTexFor:brush];
    
    glDisable(GL_DEPTH_TEST);
    glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glDeleteTextures(1, &currentTex);
    glDeleteTextures(1, &previousTex);
    glDeleteTextures(1, &backgroundTex);
}

- (void)destroyBrushTexture {
    glDeleteTextures(1, &brushTex[0]);
    glDeleteTextures(1, &brushTex[1]);
    glDeleteTextures(1, &brushTex[2]);
}

- (void)cleanTexFor:(Frame *)frame {
	GLuint tex = frame.tex;
	glDeleteTextures(1, &tex);
	frame.tex = 0;
}

- (void)setTex:(GLuint)tex Size:(CGSize)size WithBuffer:(const void*)buffer
{
    glBindTexture(GL_TEXTURE_2D, tex);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);		
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	glBindTexture(GL_TEXTURE_2D, 0); 
}

- (GLuint)initNewTex {
	GLuint tex;
	glGenTextures(1, &tex);
//	glBindTexture(GL_TEXTURE_2D, tex);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, PaperSizeWidth, PaperSizeHeight, 0,  GL_RGBA, GL_UNSIGNED_BYTE, NULL);	
//	glBindTexture(GL_TEXTURE_2D, 0);

    [self setTex:tex Size:CGSizeMake(PaperSizeWidth, PaperSizeHeight) WithBuffer:NULL];
	return tex;
}

- (void)setTex:(GLuint)tex WithImage:(UIImage *)image 
{
	CGImageRef textureImage = image.CGImage;
	
	NSInteger texWidth = CGImageGetWidth(textureImage);
    NSInteger texHeight = CGImageGetHeight(textureImage);
	NSLog(@"image %d, %d", texWidth, texHeight);
	
	GLubyte *buffer = (GLubyte *) calloc(texWidth * texHeight * 4, sizeof(GLubyte));
	GLubyte *textureData = (GLubyte *) calloc(texWidth * texHeight * 4, sizeof(GLubyte));
	
	CGContextRef textureContext = CGBitmapContextCreate(buffer,
														texWidth,
														texHeight,
														8, texWidth * 4,
														CGImageGetColorSpace(textureImage),
														kCGImageAlphaPremultipliedLast);
	
	CGContextDrawImage(textureContext, CGRectMake(0.0, 0.0, (float)texWidth, (float)texHeight), textureImage);
	
    for(int y = 0; y <texHeight; y++)
    {
        for(int x = 0; x <texWidth * 4; x++)
        {
            textureData[(texHeight - 1 - y) * texWidth * 4 + x] = buffer[y * 4 * texWidth + x];
        }
    }
	
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
	glBindTexture(GL_TEXTURE_2D, 0);
	free(buffer);
	free(textureData);
}

- (void)setTex:(GLuint)tex WithImage:(UIImage *)image reversion:(BOOL)rev
{
	CGImageRef textureImage = image.CGImage;
	
	NSInteger texWidth = CGImageGetWidth(textureImage);
    NSInteger texHeight = CGImageGetHeight(textureImage);
	NSLog(@"image %d, %d", texWidth, texHeight);
	
	GLubyte *buffer = (GLubyte *) calloc(texWidth * texHeight * 4, sizeof(GLubyte));
    
    CGContextRef textureContext = CGBitmapContextCreate(buffer,
                                                        texWidth,
                                                        texHeight,
                                                        8, texWidth * 4,
                                                        CGImageGetColorSpace(textureImage),
                                                        kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(textureContext, CGRectMake(0.0, 0.0, (float)texWidth, (float)texHeight), textureImage);
    
    if(rev){
      	GLubyte *textureData = (GLubyte *) calloc(texWidth * texHeight * 4, sizeof(GLubyte));
    
        for(int y = 0; y <texHeight; y++)
        {
            for(int x = 0; x <texWidth * 4; x++)
            {
                textureData[(texHeight - 1 - y) * texWidth * 4 + x] = buffer[y * 4 * texWidth + x];
            }
        }
        
        [self setTex:tex Size:CGSizeMake(texWidth, texHeight) WithBuffer:textureData];
        free(textureData);
    }
    else {
        [self setTex:tex Size:CGSizeMake(texWidth, texHeight) WithBuffer:buffer];   
    }
    
	free(buffer);
}

- (void)setTex:(GLuint)tex WithLabel:(NSString *)label {
	NSLog(@"to load texFile %@", label);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.dat", label]];
    	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:datFilePath]){
		NSData *data = [NSData dataWithContentsOfFile:datFilePath];
//		[self setTex:tex WithBuffer:[data bytes]];
        [self setTex:tex Size:CGSizeMake(PaperSizeWidth, PaperSizeHeight) WithBuffer:[data bytes]];
		
        return;
	}
    else {
        [self cleanColor:Color4Make(0.0, 0.0, 0.0, 0.0) TextureContext:tex];
    }
	
	NSLog(@"load texFile no file");
}

- (void)setTex:(GLuint)tex WithTex:(GLuint)sourceTex {
    GLuint tempTex = [self initNewTex];
	[self bufferBlindTex:tempTex];
	GLubyte *buffer = [self readPixFromBuffer:unvisibleFrameBuffer Blind:sourceTex];
//    [self setTex:tex WithBuffer:buffer];
    [self setTex:tex Size:CGSizeMake(PaperSizeWidth, PaperSizeHeight) WithBuffer:buffer];
	    
    glDeleteTextures(1, &tempTex);
	free(buffer);
}

- (BOOL)bufferBlindTex:(GLuint)tex 
{
	if(unvisibleFrameBuffer == 0){
		glGenFramebuffersOES(1, &unvisibleFrameBuffer);
	}
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, unvisibleFrameBuffer);   	  
	glBindTexture(GL_TEXTURE_2D, tex);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, PaperSizeWidth, PaperSizeHeight, 0,  GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glFramebufferTexture2DOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, tex, 0);
	
	GLuint status = glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES);
	if (status != GL_FRAMEBUFFER_COMPLETE_OES)
	{
		NSLog(@"NOooooooommmmmmmmmmm");
		return NO;
	}
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	return YES;
}

- (void)cleanColor:(Color4)color TextureContext:(GLuint)tex {
    [self bufferBlindTex:tex];
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, unvisibleFrameBuffer);
	glClearColor(color.r, color.g, color.b, color.a);
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, 0);
}

- (void)renderLineToBuffer:(GLuint)buffer Brush:(Frame *)penBrush FromPoint:(CGPoint)start toPoint:(CGPoint)end {
    static GLfloat*		vertexBuffer = NULL;
	static NSUInteger	vertexMax = 64;
	NSUInteger			vertexCount = 0,
	count,
	i;
	
	// Convert locations from Points to Pixels
	CGFloat scale = 1.0;
	start.x *= scale;
	start.y *= scale;
	end.x *= scale;
	end.y *= scale;
	
	// Allocate vertex array buffer
	if(vertexBuffer == NULL)
		vertexBuffer = (GLfloat *)(malloc(vertexMax * 2 * sizeof(GLfloat)));
	
	// Add points to the buffer so there are drawing points every X pixels
	count = MAX(ceilf(sqrtf((end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y)) / kBrushPixelStep), 1);
	for(i = 0; i < count; ++i) {
		if(vertexCount == vertexMax) {
			vertexMax = 2 * vertexMax;
			vertexBuffer = (GLfloat *)(realloc(vertexBuffer, vertexMax * 2 * sizeof(GLfloat)));
		}
		
		vertexBuffer[2 * vertexCount + 0] = start.x + (end.x - start.x) * ((GLfloat)i / (GLfloat)count);
		vertexBuffer[2 * vertexCount + 1] = start.y + (end.y - start.y) * ((GLfloat)i / (GLfloat)count);
		vertexCount += 1;
	}
    
    glEnable(GL_POINT_SPRITE_OES);
    glTexEnvf(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, buffer);
	
    glPointSize(penBrush.size);
    
    if(penBrush.tex == brushTex[2]){
       	glColor4f(0.0, 0.0, 0.0, 0.0);              
    }
    else {
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
       	glColor4f(penBrush.color.r, penBrush.color.g, penBrush.color.b, penBrush.color.a);     
    }
    glBindTexture(GL_TEXTURE_2D, penBrush.tex);
	glPushMatrix();
    
    glScalef(1/penBrush.transform.scale.x, 1/penBrush.transform.scale.y, 1.0);
    glRotatef(penBrush.transform.angle*180.0/M_PI, 0, 0, 1);
    glTranslatef(-penBrush.transform.translate.x, -penBrush.transform.translate.y, 0.0);
    
	glVertexPointer (2, GL_FLOAT, 0, vertexBuffer); 
	glDrawArrays(GL_POINTS, 0, vertexCount);
	glPopMatrix();
    
    glDisable(GL_BLEND);	
    
    if(buffer == visibleFrameBuffer){
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, visibleRenderBuffer);
		[context presentRenderbuffer:GL_RENDERBUFFER_OES];
	}
}

- (void)renderToBuffer:(GLuint)buffer Texture:(GLuint)tex Frame:(Frame *)f
{
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, buffer);
    
    if(f){
        [self renderTex:tex Transform:f.transform Color:whiteColor];        
    }
    else {
        [self renderTex:tex Transform:originTrans Color:whiteColor];
    }
    
    if(buffer == visibleFrameBuffer){
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, visibleRenderBuffer);
		[context presentRenderbuffer:GL_RENDERBUFFER_OES];
	}
}

- (void)renderFrame:(Frame *)frame PervFrame:(Frame *)pervFrame AndBG:(Frame *)bgFrame To:(GLuint)buffer  
{

	glBindFramebufferOES(GL_FRAMEBUFFER_OES, buffer);
	glClearColor(ColorOne);
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
//    [self renderTex:backgroundTex Transform:bgFrame.transform Color:whiteColor];
    [self renderBGTex:backgroundTex Transform:bgFrame.transform Color:whiteColor];
	glEnable(GL_BLEND);
    
    if(isShowPreviousFrame) {
//        glBlendFunc(GL_ONE, GL_DST_ALPHA);
        glBlendFunc(GL_ONE, GL_DST_COLOR);
        [self renderTex:previousTex Transform:pervFrame.transform Color:Color4Make(0.5, 0.5, 0.5, 0.5)];	        
    }
    
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    [self renderTex:currentTex Transform:frame.transform Color:whiteColor];
    
	glDisable(GL_BLEND);
	
	if(buffer == visibleFrameBuffer){
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, visibleRenderBuffer);
		[context presentRenderbuffer:GL_RENDERBUFFER_OES];
	}
}

- (void)saveTex:(GLuint)tex forFrame:(Frame *)f {
    
	GLuint tempTex = [self initNewTex];
	[self bufferBlindTex:tempTex];
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, unvisibleFrameBuffer);  
	
	glClearColor(ColorOne);
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    [self renderTex:tex Transform:originTrans Color:whiteColor];
	glDisable(GL_BLEND);
	
	NSInteger myDataLength = PaperSizeWidth * PaperSizeHeight * 4;     
	GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, PaperSizeWidth, PaperSizeHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);	
	NSMutableData *data = [NSMutableData dataWithBytes:buffer length:myDataLength];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.dat", f.texLabel]];	
	[data writeToFile:datFilePath atomically:YES];
	
	glDeleteTextures(1, &tempTex);
	free(buffer);
}

- (void)initBackgroundWith:(UIImage *)image {
    
//    [self setTex:backgroundTex WithImage:image];
    [self setTex:backgroundTex WithImage:image reversion:NO];
}

- (void)initBackgroundWithColor:(Color4)color {
    
    [self cleanColor:color TextureContext:backgroundTex];    
}

- (void)initBackgroundWithInfo {
	
	NSDictionary *dic = [self currentWorkInfo];
    [self setTex:backgroundTex WithLabel:[dic objectForKey:@"background"]];
}

- (void)saveBackground {
	GLuint tempTex = [self initNewTex];
	[self bufferBlindTex:tempTex];
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, unvisibleFrameBuffer);  
	
	glClearColor(ColorOne);
	glClearDepthf(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    [self renderTex:backgroundTex Transform:originTrans Color:whiteColor];
	glDisable(GL_BLEND);
	
	NSInteger myDataLength = PaperSizeWidth * PaperSizeHeight * 4;     
	GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, PaperSizeWidth, PaperSizeHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);	
	NSMutableData *data = [NSMutableData dataWithBytes:buffer length:myDataLength];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSDictionary *dic = [self currentWorkInfo];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.dat", [dic objectForKey:@"background"]]];	
	[data writeToFile:datFilePath atomically:YES];
	
	glDeleteTextures(1, &tempTex);
	free(buffer);
}

void releaseScreenshotData(void *info, const void *data, size_t size) {
	free((void *)data);
};

- (UIImage *)screenshotFor:(Frame *)f Prev:(Frame *)preF AndBG:(Frame *)bgF 
{
	GLuint tempTex = [self initNewTex];
	[self bufferBlindTex:tempTex];
    [self renderFrame:f PervFrame:preF AndBG:bgF To:unvisibleFrameBuffer];
	UIImage *image = [self screenshotImage];
    glDeleteTextures(1, &tempTex);
    
    return image;
}

- (UIImage *)screenshotImage {
	NSInteger myDataLength = backingWidth * backingHeight * 4;
	
	// allocate array and read pixels into it.
	GLuint *buffer = (GLuint *) malloc(myDataLength);
	glReadPixels(0, 0, backingWidth, backingHeight, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	
	// gl renders “upside down” so swap top to bottom into new array.
	for(int y = 0; y < backingHeight / 2; y++) {
		for(int x = 0; x < backingWidth; x++) {
			//Swap top and bottom bytes
			GLuint top = buffer[y * backingWidth + x];
			GLuint bottom = buffer[(backingHeight - 1 - y) * backingWidth + x];
			buffer[(backingHeight - 1 - y) * backingWidth + x] = top;
			buffer[y * backingWidth + x] = bottom;
		}
	}
	
	// make data provider with data.
	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer, myDataLength, releaseScreenshotData);
	
	// prep the ingredients
	const int bitsPerComponent = 8;
	const int bitsPerPixel = 4 * bitsPerComponent;
	const int bytesPerRow = 4 * backingWidth;
	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
	
	// make the cgimage
	CGImageRef imageRef = CGImageCreate(backingWidth, backingHeight, bitsPerComponent, bitsPerPixel, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
	CGColorSpaceRelease(colorSpaceRef);
	CGDataProviderRelease(provider);
	
	// then make the UIImage from that
	UIImage *myImage = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	
	return myImage;
}

#pragma mark -
#pragma mark Load And Save Fuction
- (NSDictionary *)getWorkInfoAt:(NSInteger)num {
	if([worksArray count] >= num && num >= 1){
		return [worksArray objectAtIndex:num - 1];
	}
	else {
		return nil;
	}
}

- (NSDictionary *)currentWorkInfo {
	return [self getWorkInfoAt:currentWorkNum];
}

- (void)newWorkInfo {
	
	NSString *tName = @"untitled";
	NSString *cName = @"unknow";
	NSString *label = [self returnCurrentTimeString];
	NSString *BGInfo = [NSString stringWithFormat:@"bg%@", label];
	
	NSArray *infoArray = [NSArray arrayWithObjects:tName, cName, BGInfo, label, nil];
	NSArray *keyArray = [NSArray arrayWithObjects:@"title", @"creator", @"background", @"workLabel", nil];
	NSDictionary *dic = [NSDictionary dictionaryWithObjects:infoArray forKeys:keyArray];
	
	[worksArray addObject:dic];
	currentWorkNum = [worksArray count];
    
    //    [[NSFileManager defaultManager] createFileAtPath:@"/tmp/quickfox.txt" contents:nil attributes:nil];

}

- (void)writeDicsArray:(NSArray *)dicsArray toFile:(NSString *)fileName {
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	NSString *error;
	NSData *plist = [NSPropertyListSerialization dataFromPropertyList:dicsArray 
															   format:NSPropertyListXMLFormat_v1_0 
													 errorDescription:&error];
	
	[plist writeToFile:plistPath atomically:YES];
    [dicsArray writeToFile:plistPath atomically:YES];
}

- (void)loadDicsArray:(NSString *)fileName For:(NSMutableArray *)dicsArray {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		NSString *error;
		NSPropertyListFormat format;
		NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];

		[dicsArray setArray:[NSPropertyListSerialization propertyListFromData:plistXML 
															 mutabilityOption:NSPropertyListMutableContainersAndLeaves
																	   format:&format 
															 errorDescription:&error]];
	}
}
 

- (NSMutableArray *)loadDicsArray:(NSString *)fileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		NSString *error;
		NSPropertyListFormat format;
		NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	
		return [NSPropertyListSerialization propertyListFromData:plistXML 
													 mutabilityOption:NSPropertyListMutableContainersAndLeaves
															   format:&format 
													 errorDescription:&error];
	}
	return nil;
}

- (void)saveChangedTitle:(NSString *)title Creator:(NSString *)creator {
    NSDictionary *dic = [self getWorkInfoAt:currentWorkNum];
    
    NSString *tName = title;
	NSString *cName = creator;
	NSString *label = [dic objectForKey:@"workLabel"];
	NSString *BGInfo = [dic objectForKey:@"background"];
    
    NSArray *infoArray = [NSArray arrayWithObjects:tName, cName, BGInfo, label, nil];
	NSArray *keyArray = [NSArray arrayWithObjects:@"title", @"creator", @"background", @"workLabel", nil];
	dic = [NSDictionary dictionaryWithObjects:infoArray forKeys:keyArray];
    
    [worksArray replaceObjectAtIndex:currentWorkNum - 1 withObject:dic];
}

- (void)saveWorksInfo {
	[self writeDicsArray:worksArray toFile:@"workInfos"];
}

- (void)loadWorksInfo {
	[worksArray setArray:[self loadDicsArray:@"workInfos"]];
}

- (void)saveParaFor:(NSArray *)array {
	NSInteger num = [array count];
	NSMutableArray *dicsArray = [NSMutableArray array];
	
	for(int i = 0; i < num; i++){
		Frame *frame = [array objectAtIndex:i];
		[dicsArray addObject:[frame returnParaToDic]];
	}
    
	NSString *label = [[self getWorkInfoAt:currentWorkNum] objectForKey:@"workLabel"];
	[self writeDicsArray:dicsArray toFile:[NSString stringWithFormat:@"para%@",label]];
}

- (void)loadParaFor:(NSMutableArray *)array {
	NSString *label = [[self getWorkInfoAt:currentWorkNum] objectForKey:@"workLabel"];
    
	NSArray *dicsArray = [self loadDicsArray:[NSString stringWithFormat:@"para%@",label]];
	NSInteger num = [dicsArray count];	
	for(int i = 0; i < num; i++){
		NSDictionary *dic = [dicsArray objectAtIndex:i];
		Frame *newFrame = [[Frame alloc] initWithDic:dic];
		[array addObject:newFrame];
	}
    
}










-(NSString *)returnCurrentTimeString
{
    NSDateFormatter *formatter =[[[NSDateFormatter alloc] init] autorelease];
    NSDate *date = [NSDate date];
    [[NSDate date] timeIntervalSinceNow];
    [formatter setTimeStyle:NSDateFormatterFullStyle];
	[formatter setDateFormat:@"yyyyMMddHHmmss"];
	
	NSString *str = [formatter stringFromDate:date];
    return str;
}

- (BOOL) checkFileExists:(NSString *)fileName Type:(NSString *)type{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", fileName, type]];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:datFilePath]){
		return YES;
	}
	return NO;
}

- (BOOL)checkFileExists:(NSString *)fileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *plistPath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		return YES;
	}
	return NO;
}



- (void)writeImage:(UIImage *)image ToFile:(NSString *)fileName Format:(NSString *)format{
	NSData *dataImage;
	
	dataImage = UIImagePNGRepresentation(image);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
	
//	NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"png"];
	[dataImage writeToFile:datFilePath atomically:YES];
}

- (void)coalesceImagesToGif:(NSMutableArray *) imageArray File:(NSString *)fileName Delay:(CGFloat)delay{
	if(imageArray != nil){
		NSInteger num = [imageArray count];
		
		MagickWandGenesis();
		MagickWand * magick_wand;
		MagickBooleanType status;
		
		magick_wand = NewMagickWand();
		for(int i = 0; i < num; i++){
			
			NSData * dataObject = UIImagePNGRepresentation([imageArray objectAtIndex:i]);
			status = MagickReadImageBlob(magick_wand, [dataObject bytes], [dataObject length]);
			status = MagickSetImageDelay(magick_wand, delay);
		}
		magick_wand = MagickCoalesceImages(magick_wand);
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.gif", fileName]];
		
		const char *pathChar = [datFilePath UTF8String];
		status = MagickWriteImages(magick_wand, pathChar, MagickTrue);	
		
		magick_wand = DestroyMagickWand(magick_wand);
		MagickWandTerminus();		
	}
}

- (void)coalesceFramesToGif:(NSMutableArray *)framesArray File:(NSString *)fileName Delay:(CGFloat)delay {
	MagickWandGenesis();
	MagickWand * magick_wand;
	MagickBooleanType status;
	magick_wand = NewMagickWand();
	
	NSInteger num = [framesArray count];
	for(int i = 0; i < num; i++) {
		Frame *frame = [framesArray objectAtIndex:i];

        [self setTex:currentTex WithLabel:frame.texLabel];
		UIImage *image = [self backScreenshotImage:frame];
		
		NSData * dataObject = UIImagePNGRepresentation(image);
		status = MagickReadImageBlob(magick_wand, [dataObject bytes], [dataObject length]);
		status = MagickSetImageDelay(magick_wand, delay);
	}

	MagickWand *wand2 = MagickCoalesceImages(magick_wand);
	ClearMagickWand(magick_wand); 
	magick_wand = DestroyMagickWand(magick_wand);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *datFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.gif", fileName]];
	
	const char *pathChar = [datFilePath UTF8String];
	status = MagickWriteImages(wand2, pathChar, MagickTrue);
	
	ClearMagickWand(wand2); 
	wand2 = DestroyMagickWand(wand2);
	
	MagickWandTerminus();
}

- (void)setDrawBrush:(NSInteger)tag {
    brush.tex = brushTex[tag];
}

- (void)setBrushSize:(GLfloat)size {
    brush.size = size;
}

- (void)brushColorSet:(Color4)color {
    brush.color = color;
}

- (NSArray *)returnBrushValues {
    NSNumber *r = [NSNumber numberWithFloat:brush.color.r];
    NSNumber *g = [NSNumber numberWithFloat:brush.color.g];
    NSNumber *b = [NSNumber numberWithFloat:brush.color.b];
    NSNumber *a = [NSNumber numberWithFloat:brush.color.a];
    
    NSNumber *s = [NSNumber numberWithFloat:brush.size];
    NSNumber *t = [NSNumber numberWithFloat:brush.tex];
    
    NSArray *lineValue = [NSArray arrayWithObjects:r, g, b, a, s, t,nil];
    return lineValue;
}

@end