//
//  PopView.h
//  DrawCool
//
//  Created by RakuTech on 11-3-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
typedef struct
{
	CGFloat x;
	CGFloat y;
	CGFloat z;
} Axis;

@protocol PopDelegate <NSObject>
@optional
- (void)setUserInteractionEnabled:(BOOL)set;
@end

@interface PopView : UIView {

    UIButton *coverView;
    BOOL useCover;
}
@property (nonatomic, assign)BOOL useCover;

- (void)saveData;
- (void)loadData;
- (void)setUseCover:(BOOL)set;

- (void)showAnimation:(BOOL)animation;
- (void)hideAnimation:(BOOL)animation; 
- (void)coverTouched;

- (CAAnimationGroup *)rotateAxis:(Axis)axis Quarter:(CGFloat)m CycleTime:(CGFloat) cycleTime;
- (CABasicAnimation *)opacity:(CGFloat)startA To:(CGFloat)endA;
- (CABasicAnimation *)transfrom:(CGPoint)startP To:(CGPoint)endP;
- (CABasicAnimation *)scale:(CGPoint)startN To:(CGPoint)endN;
- (void)actionAnimation:(NSArray *)array ForKey:(NSString *)key;

- (void)actionAnimation:(NSArray *)array ForKey:(NSString *)key Duration:(CGFloat)time;

@end
