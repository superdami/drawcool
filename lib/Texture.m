//
//  Texture.m
//  DrawCool
//
//  Created by RakuTech on 11-1-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Texture.h"

@implementation Frame

@synthesize size;
@synthesize texLabel;
@synthesize color;
@synthesize tex; 

@synthesize transform;

- (id) init {
	if ((self = [super init])) {
		[self resetDefulat];
	}	
	return self;	
}

- (id)initWithDic:(NSDictionary *)paraDic {
	if ((self = [super init])) {
        transform.translate = CGPointFromString([paraDic objectForKey:@"translation"]);
        transform.scale = CGPointFromString([paraDic objectForKey:@"scaling"]);
        transform.angle = [[paraDic objectForKey:@"rotation"] floatValue];
		self.texLabel = [paraDic objectForKey:@"label"];
	}	
	return self;
}

- (void)dealloc {
	if(texLabel){
		[texLabel release];
	}
	[super dealloc];
}

- (NSDictionary *)returnParaToDic {
	NSArray *para = [NSArray arrayWithObjects:
                     NSStringFromCGPoint(transform.translate),
                     NSStringFromCGPoint(transform.scale),
                     [NSNumber numberWithFloat:transform.angle],
                     texLabel, nil];
    NSArray *paraKeys = [NSArray arrayWithObjects:@"translation", @"scaling", @"rotation", @"label", nil];
    
	return [NSDictionary dictionaryWithObjects:para forKeys:paraKeys];
 
}

-(NSString *)returnCurrentTimeString
{
    NSDateFormatter *formatter =[[[NSDateFormatter alloc] init] autorelease];
    NSDate *date = [NSDate date];
    [[NSDate date] timeIntervalSinceNow];
    [formatter setTimeStyle:NSDateFormatterFullStyle];
	[formatter setDateFormat:@"yyyyMMddHHmmss"];
	
	NSString *str = [formatter stringFromDate:date];
    return str;
}

static Transform a = {0.0, 0.0, 1.0, 1.0, 0.0};
- (void)resetDefulat {

    transform = a;
    
	tex = 0;
	self.texLabel = [self returnCurrentTimeString];
}

@end
