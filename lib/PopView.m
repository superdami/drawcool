//
//  PopView.m
//  DrawCool
//
//  Created by RakuTech on 11-3-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//
#import "PopView.h"


@implementation PopView
@synthesize useCover;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)saveData {
    
}

- (void)loadData {

}

- (void)dealloc
{
    [super dealloc];
}

- (void)setUseCover:(BOOL)set {
    if(set && !useCover){
        coverView = [UIButton buttonWithType:UIButtonTypeCustom];
        coverView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
        [self addSubview:coverView];
        [self sendSubviewToBack:coverView];
        coverView.frame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
        [coverView addTarget:self action:@selector(coverTouched) forControlEvents:UIControlEventTouchUpInside];
        
        useCover = YES;
    }
    
    if(!set){
        [coverView removeFromSuperview];
        useCover = NO;
    }
}

#define showTime 0.2
#define hideTime 0.2

- (CAAnimationGroup *)rotateAxis:(Axis)axis Quarter:(CGFloat)m CycleTime:(CGFloat) cycleTime{

//    CGFloat angel = m*M_PI_2;
    NSInteger n = m;
    CGFloat d = m - n;
    
    NSMutableArray *array = [NSMutableArray array];
    for(int i = 0; i < n; i++){
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform"];
        ani.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(i*M_PI_2, axis.x, axis.y, axis.z)];
        ani.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation((i + 1)*M_PI_2, axis.x, axis.y, axis.z)];
        ani.delegate = self;
        ani.duration = cycleTime;
        ani.beginTime = cycleTime*i;
        
        [array addObject:ani];
    }
    
    if(d > 0){
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform"];
        ani.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(n*M_PI_2, axis.x, axis.y, axis.z)];
        ani.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation( m*M_PI_2, axis.x, axis.y, axis.z)];
        ani.delegate = self;
        ani.duration = cycleTime*d;
        ani.beginTime = cycleTime*n;
        
        [array addObject:ani];
    }
    
    CAAnimationGroup *aniGroup = [CAAnimationGroup animation];
    aniGroup.animations = array;
    aniGroup.duration = m*cycleTime;
    aniGroup.delegate = self;
    aniGroup.removedOnCompletion = NO;
    aniGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    return aniGroup;
}

- (CABasicAnimation *)opacity:(CGFloat)startA To:(CGFloat)endA {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"opacity"];
    ani.fromValue = [NSNumber numberWithFloat:startA];
    ani.toValue = [NSNumber numberWithFloat:endA];
    ani.duration = showTime;
    ani.delegate = self;

    return ani;
}

- (CABasicAnimation *)transfrom:(CGPoint)startP To:(CGPoint)endP {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform"];
    ani.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(startP.x, startP.y, 0.0)];
    ani.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(endP.x, endP.y, 0.0)];
    ani.duration = showTime;
    ani.delegate = self;
    
    return ani;
}

- (CABasicAnimation *)scale:(CGPoint)startN To:(CGPoint)endN {
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform"];
    ani.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(startN.x, startN.y, 1.0)];
    ani.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(endN.x, endN.y, 1.0)];
    ani.duration = showTime;
    ani.delegate = self;
    
    return ani;
}

- (void)actionAnimation:(NSArray *)array ForKey:(NSString *)key{
    
    CAAnimationGroup *aniGroup = [CAAnimationGroup animation];
    aniGroup.animations = array;
    aniGroup.duration = showTime;
    aniGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    aniGroup.delegate = self;
    aniGroup.removedOnCompletion = NO;
    
    [self.layer addAnimation:aniGroup forKey:key];
    
    self.transform = CGAffineTransformIdentity;
}

- (void)actionAnimation:(NSArray *)array ForKey:(NSString *)key Duration:(CGFloat)time{
    
    CAAnimationGroup *aniGroup = [CAAnimationGroup animation];
    aniGroup.animations = array;
    aniGroup.duration = time;
    aniGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    aniGroup.delegate = self;
    aniGroup.removedOnCompletion = NO;
    
    [self.layer addAnimation:aniGroup forKey:key];
    
    self.transform = CGAffineTransformIdentity;
}

- (void)showAnimation:(BOOL)animation {
    if(animation){
        CABasicAnimation *ani0 = [self opacity:0.0 To:1.0];
        NSArray *array = [NSArray arrayWithObjects:ani0, nil];
        [self actionAnimation:array ForKey:@"show"];
        
        self.alpha = 1.0;
    }
}

- (void)hideAnimation:(BOOL)animation {
    if(animation){
        CABasicAnimation *ani0 = [self opacity:1.0 To:0.0];
        NSArray *array = [NSArray arrayWithObjects:ani0, nil];
        [self actionAnimation:array ForKey:@"hide"];
        
        self.alpha = 0.0;
    }
    else {
        [self removeFromSuperview];
    }
}

- (void)coverTouched {
    [self hideAnimation:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}
@end
