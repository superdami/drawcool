//
//  Center.h
//  DrawCool
//
//  Created by RakuTech on 11-2-12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>

#import "Texture.h"
#import "MagickWand.h"

@interface Center : NSObject {
	EAGLContext *context;
	GLint backingWidth, backingHeight;
	
//	Frame *background;
	
	GLuint visibleFrameBuffer, visibleRenderBuffer, depthRenderbuffer;	
	GLuint unvisibleFrameBuffer;
	
	NSMutableArray *worksArray;
	NSInteger currentWorkNum;
    
    Frame *brush;
    GLuint brushTex[3];

    GLuint currentTex, previousTex;
    GLuint backgroundTex;
    
    BOOL isShowPreviousFrame;
}

@property (nonatomic, retain)EAGLContext *context;
@property (nonatomic, assign)GLuint visibleFrameBuffer;	
@property (nonatomic, assign)GLuint unvisibleFrameBuffer;
@property (nonatomic, assign)GLuint currentTex, previousTex;

@property (nonatomic, assign)NSInteger currentWorkNum;
@property (nonatomic, retain)NSMutableArray *worksArray;

@property (nonatomic, readonly)Frame *brush;
//@property (nonatomic, retain)Frame *background;
@property (nonatomic, assign)BOOL isShowPreviousFrame;

- (id) init;

- (BOOL)createFramebuffer:(UIView *)view;
- (void)createBrushTexture;
- (void)destroyFramebuffer;
- (void)destroyBrushTexture;
- (void)cleanTexFor:(Frame *)frame;

- (GLuint)initNewTex;
 
- (void)setTex:(GLuint)tex WithImage:(UIImage *)image;
- (void)setTex:(GLuint)tex WithLabel:(NSString *)label;
- (void)setTex:(GLuint)tex WithTex:(GLuint)sourceTex;

- (BOOL)bufferBlindTex:(GLuint)tex;
- (void)cleanColor:(Color4)color TextureContext:(GLuint)tex;
- (void)renderLineToBuffer:(GLuint)buffer Brush:(Frame *)penBrush FromPoint:(CGPoint)start toPoint:(CGPoint)end;
- (void)renderToBuffer:(GLuint)buffer Texture:(GLuint)tex Frame:(Frame *)f;
- (void)renderFrame:(Frame *)frame PervFrame:(Frame *)pervFrame AndBG:(Frame *)bgFrame To:(GLuint)buffer;
- (void)saveTex:(GLuint)tex forFrame:(Frame *)f;

- (void)initBackgroundWith:(UIImage *)image;
- (void)initBackgroundWithColor:(Color4)color;
- (void)initBackgroundWithInfo;
- (void)saveBackground;

- (UIImage *)screenshotFor:(Frame *)f Prev:(Frame *)preF AndBG:(Frame *)bgF; 
//- (UIImage *)backScreenshotImage:(Frame *)frame;
- (UIImage *)screenshotImage;



- (NSDictionary *)currentWorkInfo;
- (void)newWorkInfo ;
- (void)writeDicsArray:(NSArray *)dicsArray toFile:(NSString *)fileName;

- (void)saveChangedTitle:(NSString *)title Creator:(NSString *)creator;
- (void)saveWorksInfo;
- (void)loadWorksInfo;
- (void)saveParaFor:(NSArray *)array;
- (void)loadParaFor:(NSArray *)array;

- (NSString *)returnCurrentTimeString;
- (BOOL)checkFileExists:(NSString *)fileName Type:(NSString *)type;
- (BOOL)checkFileExists:(NSString *)fileName;



- (void)writeImage:(UIImage *)image ToFile:(NSString *)fileName Format:(NSString *)format;
- (void)coalesceImagesToGif:(NSMutableArray *) imageArray File:(NSString *)fileName Delay:(CGFloat)delay;
- (void)coalesceFramesToGif:(NSMutableArray *)framesArray File:(NSString *)fileName Delay:(CGFloat)delay;



- (void)setDrawBrush:(NSInteger)tag;
- (void)setBrushSize:(GLfloat)size;
- (NSArray *)returnBrushValues;
- (void)brushColorSet:(Color4)color;
@end
