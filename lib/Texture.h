//
//  Texture.h
//  DrawCool
//
//  Created by RakuTech on 11-1-17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
typedef struct
{
	GLfloat r;
	GLfloat g;
	GLfloat b;
	GLfloat a;
} Color4;

typedef struct 
{
    CGPoint translate;
    CGPoint scale;
    GLfloat angle;
} Transform;

static inline Color4 Color4Make(GLfloat inR, GLfloat inG, GLfloat inB, GLfloat inA )
{
    Color4 c;
    c.r = inR;
    c.g = inG;
    c.b = inB;
    c.a = inA;
    return c;
}

static inline void SetTranslate(CGPoint *t, CGPoint origin, GLfloat angle, GLfloat scale)
{
    *t = CGPointMake(t->x - origin.x, t->y - origin.y);
    *t = CGPointMake(cos(angle)*t->x+sin(angle)*t->y, -sin(angle)*t->x+cos(angle)*t->y);
	*t = CGPointMake(origin.x + t->x, origin.y + t->y);
    
    *t = CGPointMake(origin.x - (origin.x - t->x)*scale, origin.y - (origin.y - t->y)*scale);
}

static inline void SetTransform(Transform *t, CGPoint origin, CGPoint movePath, GLfloat angle, GLfloat scale)
{
    t->translate = CGPointMake(t->translate.x + movePath.x, t->translate.y + movePath.y);
    t->scale = CGPointMake(t->scale.x*scale, t->scale.y*scale);
    t->angle = t->angle + angle;
    
    SetTranslate(&t->translate, origin, angle, scale);    
}

static inline void RevertTranslate(CGPoint *t, Transform transform)
{
    SetTranslate(t, transform.translate, -transform.angle, 1/transform.scale.x);
    *t = CGPointMake(t->x - transform.translate.x, t->y - transform.translate.y);    
}

@interface Frame : NSObject{
    
    Transform transform;
    
	NSString *texLabel;
    CGFloat size;
	
	Color4 color;
	GLuint tex;
}

@property (nonatomic, assign)CGFloat size;
@property (nonatomic, retain)NSString *texLabel;
@property (nonatomic, assign)Color4 color;
@property (nonatomic, assign)GLuint tex;
@property (nonatomic, assign)Transform transform;

- (id)init;
- (id)initWithDic:(NSDictionary *)paraDic;
- (NSDictionary *)returnParaToDic;
- (void)resetDefulat;
@end	
