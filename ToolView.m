//
//  ToolView.m
//  DrawCool
//
//  Created by RakuTech on 11-6-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ToolView.h"
#define hideDepth 30.0

@implementation ToolView
@synthesize mainScroller;
@synthesize delegate;

@synthesize switchButton;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.mainScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(60.0, 0.0, self.frame.size.width - 60.0, self.frame.size.height - hideDepth)];
        [mainScroller setContentSize:CGSizeMake(mainScroller.frame.size.width, mainScroller.frame.size.height*2)];
        mainScroller.delegate = self;
        mainScroller.pagingEnabled = YES;
        mainScroller.scrollEnabled = NO;
        [self addSubview:mainScroller];
        [mainScroller release]; 
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [mainScroller release];
    [switchButton release];
    [super dealloc];
}

- (void)switchMode:(BOOL)isPreview {
    if(isPreview){
        [mainScroller setContentOffset:CGPointMake(0.0, mainScroller.frame.size.height) animated:YES];
        [switchButton setBackgroundImage:[UIImage imageNamed:@"showToolBarButton"] forState:UIControlStateNormal];        
    }
    else {
        [mainScroller setContentOffset:CGPointMake(0.0, 0.0) animated:YES];
        [switchButton setBackgroundImage:[UIImage imageNamed:@"playAnimViewButton"] forState:UIControlStateNormal];
    }
    
    Axis a = {1.0, 0.0, 0.0};
    CAAnimationGroup *aniGroup = [self rotateAxis:a Quarter:2 CycleTime:0.1];
    
    [switchButton.layer addAnimation:aniGroup forKey:@"switch"];
}
@end
