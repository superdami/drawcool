//
//  MainController.h
//  DrawCool
//
//  Created by RakuTech on 10-10-28.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Center.h"
#import "PenBox.h"
#import "DrawView.h"
#import "InfoView.h"
#import "PaletteView.h"

#import "MagickWand.h"

@interface MainController : UIViewController <DrawViewDelegate, InfoViewDelegate, PaletteViewDelegate, PenBoxDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
	Center *center;
	DrawView *drawView;
    
    UIView *playControl, *drawControl;
    PenBox *penControl;
    PaletteView *colorControl, *backgroundControl;
    PopView *topPopView;
    
}
@property (nonatomic, retain)Center *center;

@property (nonatomic, retain)IBOutlet DrawView *drawView;
@property (nonatomic, retain)IBOutlet UIView *playControl, *drawControl;

- (IBAction)drawControl:(id)sender;
- (IBAction)backToRoot;
- (IBAction)callTopPopView;

@end
