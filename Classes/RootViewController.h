//
//  RootViewController.h
//  DrawCool
//
//  Created by RakuTech on 11-1-11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainController;
@class Center;
@interface RootViewController : UITableViewController{
	
	Center *center;

}

- (IBAction)add; 
- (IBAction)edit;
@end
