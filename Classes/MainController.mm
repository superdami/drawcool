//
//  MainController.m
//  DrawCool
//
//  Created by RakuTech on 10-10-28.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MainController.h"



@implementation MainController
@synthesize center;
@synthesize drawView;

@synthesize playControl, drawControl;

- (void)beginGeneratingDeviceOrientationNotifications {
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:)
												 name:UIDeviceOrientationDidChangeNotification object:nil];
	[self performSelectorOnMainThread:@selector(orientationDidChange:) withObject:nil waitUntilDone:NO];
}

- (void)endGeneratingDeviceOrientationNotifications {
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (BOOL)isGeneratingDeviceOrientationNotifications {
	return [[UIDevice currentDevice] isGeneratingDeviceOrientationNotifications];
}

- (void)orientationDidChange:(NSNotification *)notification {
	
}

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
		
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	drawView.center = center;
      
    [drawControl setCenter:CGPointMake(drawView.bottomControlView.mainScroller.frame.size.width/2.0, drawView.bottomControlView.mainScroller.frame.size.height/2.0)];
    [drawView.bottomControlView.mainScroller addSubview:drawControl];
    [drawControl release];
    
    [playControl setCenter:CGPointMake(drawView.bottomControlView.mainScroller.frame.size.width/2.0, drawView.bottomControlView.mainScroller.frame.size.height*1.5)];
    [drawView.bottomControlView.mainScroller addSubview:playControl];
    [playControl release];
    
	[self beginGeneratingDeviceOrientationNotifications];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if([[self modalViewController] class] != [UIImagePickerController class]){
        NSDictionary *infoDic = [center currentWorkInfo];
        NSString *paraFile = [NSString stringWithFormat:@"para%@",[infoDic objectForKey:@"workLabel"]];
        
        if([center checkFileExists:paraFile]){
            [drawView loadWork];
        }
        else {
            [drawView startNewWork];
        }   
    }
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if([[self modalViewController] class] != [UIImagePickerController class]){
        
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.center = nil;
	
	self.drawView = nil;
	
	self.drawControl = nil;
    self.playControl = nil;
	[self endGeneratingDeviceOrientationNotifications];
}

- (void)dealloc {
    [drawView release];
	[center release];

    [super dealloc];
}

- (void)showInfoView {

    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubView" owner:self options:nil];
    InfoView *info = [nib objectAtIndex:2];
    [self.navigationController.view addSubview:info];
    
    info.center = center;
    info.delegate = self;
    [info setUseCover:NO];
    [info loadData];
    [info showAnimation:YES];
}

- (IBAction)drawControl:(id)sender {
	NSInteger tag = ((UIButton *)sender).tag;
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubView" owner:self options:nil];
    
    
    if(tag == 0){    
        if(colorControl){
            [colorControl hideAnimation:NO];
            colorControl = nil;
        }
        
        if(penControl){
            [penControl hideAnimation:YES];
            penControl = nil;
            return;
        }
        
        if(backgroundControl) {
            [backgroundControl hideAnimation:NO];
            backgroundControl = nil;
        }
        
        penControl = [nib objectAtIndex:0];
        penControl.center = center;
        [self.view addSubview:penControl];
        penControl.delegate = self;
        [penControl setFrame:CGRectMake(0.0, 440 - penControl.frame.size.height,
                                        penControl.frame.size.width, penControl.frame.size.height)];
        [penControl setUseCover:NO];
        [penControl loadData];
        [penControl showAnimation:YES];            
    }
    
	if(tag == 1){
        if(penControl){
            [penControl hideAnimation:NO];
            penControl = nil;
        }
        
        if(colorControl){
            [colorControl hideAnimation:YES];
            colorControl = nil;
            return;
        }
        
        if(backgroundControl) {
            [backgroundControl hideAnimation:NO];
            backgroundControl = nil;
        }

        colorControl = [nib objectAtIndex:1];
        
        [self.view addSubview:colorControl];
        colorControl.center = center;
        colorControl.isPenColor = YES;
        colorControl.delegate = self;
        [colorControl setFrame:CGRectMake(0.0, 440 - colorControl.frame.size.height,
                                          colorControl.frame.size.width, colorControl.frame.size.height)];
        [colorControl setUseCover:NO];
        [colorControl loadData];
        [colorControl showAnimation:YES];
	}
    
    if(tag == 2){
        [drawView undo];
        
    }
    
    if(tag == 3) {
        if(penControl){
            [penControl hideAnimation:NO];
            penControl = nil;
        }
        
        if(colorControl){
            [colorControl hideAnimation:NO];
            colorControl = nil;
        }
        
        if(backgroundControl) {
            [backgroundControl hideAnimation:YES];
            backgroundControl = nil;
            return;
        }
        
        backgroundControl = [nib objectAtIndex:1];
        
        [self.view addSubview:backgroundControl];
        backgroundControl.center = center;
        backgroundControl.isPenColor = NO;
        backgroundControl.delegate = self;
        [backgroundControl setFrame:CGRectMake(0.0, 440 - backgroundControl.frame.size.height,
                                               backgroundControl.frame.size.width, backgroundControl.frame.size.height)];
        [backgroundControl setUseCover:NO];
        [backgroundControl loadData];
        [backgroundControl showAnimation:YES];    
    }
}

- (IBAction)callTopPopView {
    if(topPopView) {
        [topPopView hideAnimation:YES];
        topPopView = nil;
        return;        
    }
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubView" owner:self options:nil];
    if([drawView isPreviewMode]) {
        
    }
    else {
        topPopView = [nib objectAtIndex:3];
        [topPopView setFrame:CGRectMake(0.0, drawView.topControlView.frame.size.height, topPopView.frame.size.width, topPopView.frame.size.height)];
        [self.view addSubview:topPopView];
        [topPopView showAnimation:YES];
    }
}

- (IBAction)backToRoot {
    [drawView forceStopPlay];
    [drawView saveWork];
    [drawView cleanAllObj];
    
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (IBAction)topViewButtons:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    
    if([drawView isPreviewMode]) {
        
    }
    else {
        if(tag == 0) {
            [drawView.topControlView addFrame];
        }
        
        if(tag == 1) {
            [drawView.topControlView copyFrame];
        }
        
        if(tag == 2) {
            [drawView.topControlView removeFrame];
        }
    }
    
    if(topPopView) {
        [topPopView hideAnimation:YES];
        topPopView = nil;
        return;        
    }
}
#pragma mark -
#pragma mark DrawViewDelegate
- (void)hideDrawControlSubview
{
    if(penControl){
        [penControl hideAnimation:YES];
        penControl = nil;        
    }
    if(colorControl){
        [colorControl hideAnimation:YES];
        colorControl = nil;
    }
}

#pragma mark -
#pragma mark PopDelegate
- (void)setUserInteractionEnabled:(BOOL)set {
    self.view.userInteractionEnabled = set;
    [self navigationController].view.userInteractionEnabled = set;
}

#pragma mark -
#pragma mark InfoViewDelegate
- (void)pushToPhotoLib {
    
    UIImagePickerController *imagePickerController = [[[UIImagePickerController alloc] init] autorelease];
	imagePickerController.delegate = self;	
	imagePickerController.allowsEditing = YES;	
	imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;	
	[self presentModalViewController:imagePickerController animated:YES];
 
    
}

- (void)willReturnToDrawview {
//    [drawView reflashCurrentRender];
}

#pragma mark -
#pragma mark paletteViewDelegate
- (void)rendView {
//    [drawView reflashFrame:drawView.currentFrameNum];
    [drawView reflashAndRend];
}

- (void)colorDidSet:(Color4)color {
    [center brushColorSet:color];
}

- (Color4)returnColor {
    return center.brush.color;
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
	
	[EAGLContext setCurrentContext:center.context];
	[center initBackgroundWith:image];
	[center saveBackground];
	
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissModalViewControllerAnimated:YES];
}

@end
