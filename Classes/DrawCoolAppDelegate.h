//
//  DrawCoolAppDelegate.h
//  DrawCool
//
//  Created by RakuTech on 10-8-24.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawCoolAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	
	UINavigationController *navigationController;
	
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@end

