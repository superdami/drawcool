//
//  PaletteView.m
//  DrawCool
//
//  Created by RakuTech on 11-3-14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PaletteView.h"


@implementation PaletteView
@synthesize delegate;
@synthesize center;
@synthesize colorSelected;
@synthesize isPenColor;

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code.
        colorScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, 280.0, self.frame.size.height)];
        [colorScroller setDelegate:self];
        colorScroller.showsHorizontalScrollIndicator = NO;
        colorScroller.bounces = NO;
        [self addSubview:colorScroller];
        [colorScroller release];
        
        UIImageView *colorPalette = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"colorPalette"]];
        [colorScroller addSubview:colorPalette];
        [colorPalette release];
        [colorScroller setContentSize:CGSizeMake(colorPalette.frame.size.width, colorScroller.frame.size.height)];
        
        colorSelectedBox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"colorSelectBox"]];
        [colorScroller addSubview:colorSelectedBox];
        [colorSelectedBox release];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [colorScroller addGestureRecognizer:tapGesture];
        [tapGesture setDelegate:self];
        [tapGesture setNumberOfTapsRequired:1];
        [tapGesture setNumberOfTouchesRequired:1];
        [tapGesture release];
        
        switchShowFrameButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [switchShowFrameButton setFrame:CGRectMake(280.0, 0.0, 40, self.frame.size.height)];
        [switchShowFrameButton setImage:[UIImage imageNamed:@"switchTwoPaper"] forState:UIControlStateNormal];
        [switchShowFrameButton addTarget:self action:@selector(switchShowHideFrame) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:switchShowFrameButton];
        switchShowFrameButton.alpha = 0.0;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetLineWidth(context, 1);
	
	CGContextMoveToPoint(context, 230.0, 330.0);
	CGContextAddLineToPoint(context, 300.0, 330.0);
	CGContextAddLineToPoint(context, 300.0, 400.0);
	CGContextAddLineToPoint(context, 230.0, 400.0);
	
	
	CGContextFillPath(context);
	CGContextStrokePath(context);
}

- (void)setIsPenColor:(BOOL)isPen {
    isPenColor = isPen;
    if(isPenColor) {
        switchShowFrameButton.alpha = 0.0;
    }
    else {
        switchShowFrameButton.alpha = 1.0;
    }
}

- (void)saveData {
    
}

- (void)loadData {
    
    [self setNeedsDisplay];
}

- (void)dealloc {
    [center release];
    [super dealloc];
}

- (void)switchShowHideFrame {
    if(center.isShowPreviousFrame) {
        center.isShowPreviousFrame = NO;
    }
    else {
        center.isShowPreviousFrame = YES;
    }
    
    [delegate rendView];
}

- (void)showAnimation:(BOOL)animation {
    if(animation){
        CABasicAnimation *ani0 = [self opacity:0.0 To:1.0];
        NSArray *array = [NSArray arrayWithObjects:ani0, nil];
        [self actionAnimation:array ForKey:@"show"];
        
        self.alpha = 1.0;
    }
}

- (void)hideAnimation:(BOOL)animation {
    
    if(animation){
        CABasicAnimation *ani0 = [self opacity:1.0 To:0.0];
        NSArray *array = [NSArray arrayWithObjects:ani0, nil];
        [self actionAnimation:array ForKey:@"hide"];
        
        self.alpha = 0.0;
    }
    else {
        [self removeFromSuperview];
    }
}

- (void)animationDidStart:(CAAnimation *)anim {
//    self.userInteractionEnabled = NO;
//    [delegate setUserInteractionEnabled:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
//    self.userInteractionEnabled = YES;
//    [delegate setUserInteractionEnabled:YES];
    
    NSArray *a = [self.layer animationKeys];    
    NSString *key = [a objectAtIndex:0];
    [self.layer removeAllAnimations];
    
    if([key isEqualToString:@"hide"]){
        [self removeFromSuperview];
    }
}

#pragma mark UIGestureDelegate Handler
- (void)tap:(UITapGestureRecognizer *)gestureRecognizer {
    CGPoint a = [gestureRecognizer locationInView:colorScroller];
    NSInteger n = a.x/colorSelectedBox.frame.size.width;
    
    [self getPixelColorAtLocation:a];
    [delegate rendView];
    
    [colorSelectedBox setFrame:CGRectMake(n*colorSelectedBox.frame.size.width, 0.0, colorSelectedBox.frame.size.width, colorSelectedBox.frame.size.height)];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (UIColor*) getPixelColorAtLocation:(CGPoint)point {
	UIColor* color = nil;
        
    UIImage *image = [UIImage imageNamed:@"colorPalette"];
	CGImageRef inImage = image.CGImage;
    
    CGFloat x = 1.0;
    if ([image respondsToSelector:@selector(scale)]) 
        x = image.scale;

	// Create off screen bitmap context to draw the image into. Format ARGB is 4 bytes for each pixel: Alpa, Red, Green, Blue
	CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
	if (cgctx == NULL) { return nil; /* error */ }
    
    size_t w = CGImageGetWidth(inImage);
	size_t h = CGImageGetHeight(inImage);
	CGRect rect = {{0,0},{w,h}}; 
    
	// Draw the image to the bitmap context. Once we draw, the memory
	// allocated for the context for rendering will then contain the
	// raw image data in the specified color space.
	CGContextDrawImage(cgctx, rect, inImage); 
    
	// Now we can get a pointer to the image data associated with the bitmap
	// context.
	unsigned char* data = CGBitmapContextGetData (cgctx);
	if (data != NULL) {
		//offset locates the pixel in the data from x,y.
		//4 for 4 bytes of data per pixel, w is width of one row of data.
		int offset = 4*((w*round(point.y))+round(point.x))*x;
		int alpha =  data[offset];
		int red = data[offset+1];
		int green = data[offset+2];
		int blue = data[offset+3];
		NSLog(@"offset: %i colors: RGB A %i %i %i  %i",offset,red,green,blue,alpha);
		color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
        colorSelected = Color4Make((red/255.0f), (green/255.0f), (blue/255.0f), 1.0);
        if(isPenColor){
            [center brushColorSet:colorSelected];            
            [self setBackgroundColor:color];
        }
        else {
            [center initBackgroundWithColor:colorSelected];
            [center saveBackground];
        }
	}
    
	// When finished, release the context
	CGContextRelease(cgctx);
	// Free image data memory for the context
	if (data) { free(data); }
    
	return color;
}

- (CGContextRef) createARGBBitmapContextFromImage:(CGImageRef) inImage {
    
	CGContextRef    context = NULL;
	CGColorSpaceRef colorSpace;
	void *          bitmapData;
	int             bitmapByteCount;
	int             bitmapBytesPerRow;
    
	// Get image width, height. We'll use the entire image.
	size_t pixelsWide = CGImageGetWidth(inImage);
	size_t pixelsHigh = CGImageGetHeight(inImage);
    
	// Declare the number of bytes per row. Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha.
	bitmapBytesPerRow   = (pixelsWide * 4);
	bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
	// Use the generic RGB color space.
	colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
	{
		fprintf(stderr, "Error allocating color space\n");
		return NULL;
	}
    
	// Allocate memory for image data. This is the destination in memory
	// where any drawing to the bitmap context will be rendered.
	bitmapData = malloc( bitmapByteCount );
	if (bitmapData == NULL)
	{
		fprintf (stderr, "Memory not allocated!");
		CGColorSpaceRelease( colorSpace );
		return NULL;
	}
    
	// Create the bitmap context. We want pre-multiplied ARGB, 8-bits
	// per component. Regardless of what the source image format is
	// (CMYK, Grayscale, and so on) it will be converted over to the format
	// specified here by CGBitmapContextCreate.
	context = CGBitmapContextCreate (bitmapData,
									 pixelsWide,
									 pixelsHigh,
									 8,      // bits per component
									 bitmapBytesPerRow,
									 colorSpace,
									 kCGImageAlphaPremultipliedFirst);
	if (context == NULL)
	{
		free (bitmapData);
		fprintf (stderr, "Context not created!");
	}
    
	// Make sure and release colorspace before returning
	CGColorSpaceRelease( colorSpace );
    
	return context;
}
@end
