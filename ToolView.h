//
//  ToolView.h
//  DrawCool
//
//  Created by RakuTech on 11-6-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopView.h"
@protocol ToolViewDelegate <NSObject>
@required

@end

@interface ToolView : PopView <UIScrollViewDelegate, UIGestureRecognizerDelegate>{
    UIScrollView *mainScroller;
    id<ToolViewDelegate>delegate;
    
    UIButton *switchButton;
}

@property (nonatomic, retain)UIScrollView *mainScroller;
@property (nonatomic, assign)IBOutlet id<ToolViewDelegate>delegate;

@property (nonatomic, retain)IBOutlet UIButton *switchButton;
- (void)switchMode:(BOOL)isPreview;
@end
