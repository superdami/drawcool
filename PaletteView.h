//
//  PaletteView.h
//  DrawCool
//
//  Created by RakuTech on 11-3-14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Center.h"
#import "PopView.h"

@protocol PaletteViewDelegate <PopDelegate>
@required
- (void)colorDidSet:(Color4)color; 
- (void)rendView;

@end

@interface PaletteView : PopView <UIScrollViewDelegate, UIGestureRecognizerDelegate>{
	id<PaletteViewDelegate> delegate;
    UIScrollView *colorScroller;
    UIImageView *colorSelectedBox;
    UIButton *switchShowFrameButton;
    
    Center *center;
    Color4 colorSelected;
    BOOL isPenColor;
}
@property (nonatomic, assign)id<PaletteViewDelegate> delegate;
@property (nonatomic, retain)Center *center;
@property (nonatomic, assign)Color4 colorSelected;
@property (nonatomic, assign)BOOL isPenColor;

@end
