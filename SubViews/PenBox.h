//
//  PenBox.h
//  DrawCool
//
//  Created by RakuTech on 11-3-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopView.h"
#import "Center.h"
@protocol PenBoxDelegate <PopDelegate>
@required

@end

@interface PenBox : PopView {
    id<PenBoxDelegate>delegate;
    UISlider *drawSizeSlider;
    Center *center;
}
@property (nonatomic, retain)IBOutlet UISlider *drawSizeSlider;
@property (nonatomic, retain)Center *center;
@property (nonatomic, assign)id<PenBoxDelegate>delegate;

- (IBAction)brushSelected:(id)sender;
- (IBAction)sizeSliderChange:(id)sender;
@end
