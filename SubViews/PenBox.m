//
//  PenBox.m
//  DrawCool
//
//  Created by RakuTech on 11-3-23.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "PenBox.h"


@implementation PenBox
@synthesize drawSizeSlider;
@synthesize center;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)saveData {
    
}

- (void)loadData {
    drawSizeSlider.value = center.brush.size;
}

- (void)dealloc
{
    [drawSizeSlider release];
    [center release];
    [super dealloc];
}
/*
- (void)showAnimation:(BOOL)animation {
    if(animation){
        CABasicAnimation *ani0 = [self transfrom:CGPointMake(0.0, 480.0) To:CGPointMake(0.0, 0.0)];
        CABasicAnimation *ani1 = [self opacity:0.0 To:1.0];
        
        NSArray *array = [NSArray arrayWithObjects:ani0, ani1, nil];
        [self actionAnimation:array ForKey:@"up"];
        
        self.alpha = 1.0;
    }
}

- (void)hideAnimation:(BOOL)animation {
    if(animation){
        CABasicAnimation *ani0 = [self transfrom:CGPointMake(0.0, 0.0) To:CGPointMake(0.0, 480.0)];
        CABasicAnimation *ani1 = [self opacity:1.0 To:0.0];
        
        NSArray *array = [NSArray arrayWithObjects:ani0, ani1, nil];
        [self actionAnimation:array ForKey:@"down"];
        
        self.alpha = 0.0;
    }
    else {
        [self removeFromSuperview];
    }
}
*/
- (void)animationDidStart:(CAAnimation *)anim {
    self.userInteractionEnabled = NO;
    [delegate setUserInteractionEnabled:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    NSArray *a = [self.layer animationKeys];    
    NSString *key = [a objectAtIndex:0];
    [self.layer removeAllAnimations];

    if([key isEqualToString:@"hide"]){
        [self removeFromSuperview];
    }    
    
    self.userInteractionEnabled = YES;
    [delegate setUserInteractionEnabled:YES];
}

- (IBAction)brushSelected:(id)sender {
    [center setDrawBrush:((UIButton *)sender).tag];
}

- (IBAction)sizeSliderChange:(id)sender {
    [center setBrushSize:((UISlider *)sender).value];
}
@end
