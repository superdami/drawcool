//
//  TouchView.h
//  TouchMove
//
//  Created by RakuTech on 11-5-3.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TouchViewDelegate <NSObject>
@optional
- (BOOL)isPreviewMode;
- (void)transformOrigin:(CGPoint)origin Translation:(CGPoint)moveDistance Angle:(CGFloat)angle Scaling:(CGFloat)scale;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface TouchView : UIView <UIGestureRecognizerDelegate> {
    id<TouchViewDelegate>delegate;
    
    UIRotationGestureRecognizer *rotationGesture;
    UIPanGestureRecognizer *panGesture;
    UIPinchGestureRecognizer *pinchGesture;
    
    CGPoint origin;
    
    CGFloat rotation;
    CGPoint translation;
    CGFloat scale;
    
    NSInteger tapCount;
}
@property (nonatomic, assign)IBOutlet id<TouchViewDelegate>delegate;

@end
