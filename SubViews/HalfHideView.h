//
//  HalfHideView.h
//  Scroller
//
//  Created by RakuTech on 11-5-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopView.h"
#import "CellView.h"

@protocol HHViewDelegate <NSObject>
@required

- (NSInteger)currentFrameNum;
- (NSInteger)frameCount;

- (void)didJumpToFrame:(NSInteger)num;

- (void)willAddNewFrame;
- (void)didAddNewFrame;
- (void)willCopyAddFrame;
- (void)didCopyAddFrame;
- (void)willRemoveFrame;
- (void)didRemoveFrame;
@end

typedef enum {
	ScrollType = 0,
    AddType,
    CopyType,
    RemoveType,
} ActionType;

@interface HalfHideView : PopView<UIScrollViewDelegate, UIGestureRecognizerDelegate>{
    UIScrollView *scroller;
    UIButton *callPopButton;
    UIButton *playButton;
    
    CGSize cellSize;
    CGFloat cellPosY;
    
    NSMutableArray *cells;
    NSInteger selectedCellIndex;
    NSTimer *timer;
//    NSInteger hightLightIndexNum;
   
    id<HHViewDelegate>delegate;
    ActionType actionType;
    
}

@property (nonatomic, assign)IBOutlet id<HHViewDelegate>delegate;
@property (nonatomic, retain)IBOutlet UIButton *playButton;
@property (nonatomic, retain)IBOutlet UIScrollView *scroller;

- (IBAction)addFrame;
- (IBAction)copyFrame;
- (IBAction)removeFrame;
- (IBAction)play;

- (void)stopPlay;
- (void)resetCellNum:(NSInteger) highLightedNum;
- (void)switchMode:(BOOL)isPreview;
@end
