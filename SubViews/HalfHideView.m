//
//  HalfHideView.m
//  Scroller
//
//  Created by RakuTech on 11-5-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "HalfHideView.h"

#define hideDepth 40.0
#define hightLightScale 40.0/35.0
#define hightLightIndexNum 5

@implementation HalfHideView
@synthesize delegate;
@synthesize playButton;
@synthesize scroller;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {                
        cells = [[NSMutableArray array] retain];
        
        cellSize = CGSizeMake(40.0, 35.0);        
        [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, cellSize.height*hightLightScale)];
        cellPosY = (hightLightScale - 1)*cellSize.height/2.0;
        
        scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height)];
        scroller.delegate = self;
        scroller.showsHorizontalScrollIndicator = NO;
        scroller.alpha = 1.0;
        
        [self insertSubview:scroller atIndex:1];
        
        NSInteger n = 10;
        [scroller setContentSize:CGSizeMake(n*cellSize.width, scroller.frame.size.height)];
        [scroller setContentInset:UIEdgeInsetsMake(0.0, n*cellSize.width, 0.0, n*cellSize.width)];
        for(int i = 0; i < n; i++){
            CellView *cell = [[[CellView alloc] initWithFrame:CGRectMake((i - 1)*cellSize.width, cellPosY, cellSize.width, cellSize.height)] autorelease];
            [scroller addSubview:cell];
            [cells addObject:cell];
            
            cell.label.text = [NSString stringWithFormat:@"cell%d", [cells count]];
            [cell setNormal];
        }
        selectedCellIndex = ([scroller contentOffset].x + self.frame.size.width/2)/cellSize.width;
        
       
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [scroller addGestureRecognizer:tapGesture];
        [tapGesture setDelegate:self];
        [tapGesture setNumberOfTapsRequired:1];
        [tapGesture setNumberOfTouchesRequired:1];
        [tapGesture release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    if([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }
    [cells release];
    [playButton release];
    [super dealloc];
}
/*
- (void)cellSetWithFrameNum:(NSInteger)num {
    m = num;
    
    for(int i = 0; i < [cells count]; i++){
        
        NSInteger cellNum = (i+1)%m;
        if(cellNum == 0){
            cellNum = m;
        }
        ((CellView *)[cells objectAtIndex:i]).label.text = [NSString stringWithFormat:@"%d", cellNum];
    }
    
}
*/
#pragma mark common

- (void)switchMode:(BOOL)isPreview {

}

- (void)resetCellNum:(NSInteger) highLightedNum{
    if(highLightedNum < 1)
        highLightedNum = 1;
    ((CellView *)[cells objectAtIndex:hightLightIndexNum]).label.text = [NSString stringWithFormat:@"%d", highLightedNum];
    
    for(int i = 0; i < hightLightIndexNum; i++){
        NSInteger preNum = [((CellView *)[cells objectAtIndex:hightLightIndexNum - i]).label.text integerValue] - 1;
        if(preNum < 1)
            preNum = [delegate frameCount];
        
        ((CellView *)[cells objectAtIndex:hightLightIndexNum - 1 - i]).label.text = [NSString stringWithFormat:@"%d", preNum];
    }
    
    for (int i = 0; i < [cells count] - hightLightIndexNum - 1; i++) {
        NSInteger nextNum = [((CellView *)[cells objectAtIndex:hightLightIndexNum + i]).label.text integerValue] + 1;
        if(nextNum > [delegate frameCount])
            nextNum = 1;
        
        ((CellView *)[cells objectAtIndex:hightLightIndexNum + 1 + i]).label.text = [NSString stringWithFormat:@"%d", nextNum];        
    }
    
    [[cells objectAtIndex:hightLightIndexNum] setHighLightScale:hightLightScale];
}

- (void)jumpToCell:(NSInteger)num Animated:(BOOL)animated {
    CGFloat newOffset = num*cellSize.width + cellSize.width/2.0 - scroller.frame.size.width/2.0;
    [scroller setContentOffset:CGPointMake(newOffset, 0.0) animated:animated];
    
}

- (void)show {
    
    CABasicAnimation *ani=[self transfrom:CGPointMake(self.frame.origin.x, self.frame.origin.y) To:CGPointMake(self.frame.origin.x, 0.0)];
    ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self.layer addAnimation:ani forKey:@"show"];
    [self.layer setFrame:CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height)];
}

- (void)hide {
    CABasicAnimation *ani=[self transfrom:CGPointMake(self.frame.origin.x, self.frame.origin.y + hideDepth) To:CGPointMake(self.frame.origin.x, 0.0)];
    ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    [self.layer addAnimation:ani forKey:@"hide"];
    [self.layer setFrame:CGRectMake(0.0, -hideDepth, self.frame.size.width, self.frame.size.height)];
}

#pragma mark UIScrollViewDelegate Handler
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"scroll");
//    NSLog(@"%f", [scrollView contentOffset].x);
//    NSLog(@"dis %f", [scrollView contentInset].right + [scrollView contentInset].left);
    
//    NSInteger n = ([scroller contentOffset].x + self.frame.size.width/2)/cellSize.width;
    CGFloat n = ([scroller contentOffset].x + self.frame.size.width/2)/cellSize.width;
    if(n >= 0){
        n = (NSInteger)n;
    }
    else {
        n = (NSInteger)n - 1;
    }
//    NSLog(@"%f", n);
    [[cells objectAtIndex:hightLightIndexNum] setNormal];    
    
    if(n > selectedCellIndex){
        for(int i = 0; i < n - selectedCellIndex; i++){
            CellView *headCell =[cells objectAtIndex:0];
            [headCell setFrame:CGRectMake(((CellView *)[cells lastObject]).frame.origin.x + cellSize.width, cellPosY, cellSize.width, cellSize.height)];
            [cells removeObjectAtIndex:0];
            
            NSInteger a = [((CellView *)[cells lastObject]).label.text integerValue];
            if(a >= [delegate frameCount]){
                headCell.label.text = [NSString stringWithFormat:@"%d", 1];
            }
            else{
                headCell.label.text = [NSString stringWithFormat:@"%d", a+1];
            }
            
            [cells addObject:headCell];
        }        
    }
    else if(n < selectedCellIndex){
        for(int i = 0; i < selectedCellIndex - n; i++){
            CellView *rearCell =[cells lastObject];
            [rearCell setFrame:CGRectMake(((CellView *)[cells objectAtIndex:0]).frame.origin.x - cellSize.width, cellPosY, cellSize.width, cellSize.height)];
            [cells removeLastObject];
            
            NSInteger a = [((CellView *)[cells objectAtIndex:0]).label.text integerValue];
            if(a >= 2){
                rearCell.label.text = [NSString stringWithFormat:@"%d", a-1];
            }
            else{
                rearCell.label.text = [NSString stringWithFormat:@"%d", [delegate frameCount]];
            }
            
            [cells insertObject:rearCell atIndex:0];            
        }
    }
    
    selectedCellIndex = n;
    [[cells objectAtIndex:hightLightIndexNum] setHighLightScale:hightLightScale];
    
    if([scrollView contentOffset].x > [scrollView contentSize].width + [scrollView contentInset].right - self.frame.size.width){
        [scrollView setContentInset:UIEdgeInsetsMake(0.0, [scrollView contentInset].left - 10*cellSize.width, 0.0, [scrollView contentInset].right + 10*cellSize.width)];
    }
    
    if([scrollView contentOffset].x < -[scrollView contentInset].left){
        [scrollView setContentInset:UIEdgeInsetsMake(0.0, [scrollView contentInset].left + 10*cellSize.width, 0.0, [scrollView contentInset].right - 10*cellSize.width)];
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    actionType = ScrollType;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self jumpToCell:selectedCellIndex Animated:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(!decelerate){
        [self jumpToCell:selectedCellIndex Animated:YES];   
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    NSInteger frameNum = [((CellView *)[cells objectAtIndex:hightLightIndexNum]).label.text integerValue];
    
    switch (actionType) {
        case ScrollType:
        {
            [delegate didJumpToFrame:frameNum];
            break;   
        }        
        case AddType:    
        {
            [delegate didAddNewFrame];
            break;
        }
        case CopyType:
        {
            [delegate didCopyAddFrame];
            break;
        }
        case RemoveType:
        {
            [delegate didRemoveFrame];
            break;
        }
        default:
            break;
    }
}
#pragma mark UIGestureDelegate Handler
- (void)tap:(UITapGestureRecognizer *)gestureRecognizer {
    actionType = ScrollType;
    
    CGPoint a = [gestureRecognizer locationInView:scroller];
    
    CGFloat n = a.x/cellSize.width;
    if(n >= 0){
        n = (NSInteger)n;
    }
    else {
        n = (NSInteger)n - 1;
    }
    [self jumpToCell:n Animated:YES];
}

- (void)pan:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [gestureRecognizer translationInView:[self superview]];
        if([self center].y + translation.y > 0 && [self center].y + translation.y < hideDepth){
            [self setCenter:CGPointMake([self center].x, [self center].y + translation.y)];
            [gestureRecognizer setTranslation:CGPointZero inView:[self superview]];
        }
    }
    
    if([gestureRecognizer state] == UIGestureRecognizerStateCancelled || [gestureRecognizer state] == UIGestureRecognizerStateEnded)
    {
        if([self center].y > hideDepth/2){
            [self show];
        }
        else {
            [self hide];     
        }
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

- (IBAction)addFrame {
    actionType = AddType;
    [delegate willAddNewFrame];
    [self resetCellNum:[delegate currentFrameNum]];
    [self jumpToCell:selectedCellIndex + 1 Animated:YES];
}

- (IBAction)copyFrame {
    actionType = CopyType;
    [delegate willCopyAddFrame];
    [self resetCellNum:[delegate currentFrameNum]];
    [self jumpToCell:selectedCellIndex + 1 Animated:YES];
}

- (IBAction)removeFrame {
    actionType = RemoveType;
    [delegate willRemoveFrame];
  
   
    if([delegate currentFrameNum] > [delegate frameCount]){
        [self resetCellNum:1];
        [self jumpToCell:selectedCellIndex - 1 Animated:YES];
    }
    else if([delegate currentFrameNum] == 1){
        [self resetCellNum:[delegate frameCount]];
        [self jumpToCell:selectedCellIndex + 1 Animated:YES];
    }
    else {
        [self resetCellNum:[delegate currentFrameNum]];
        [self jumpToCell:selectedCellIndex - 1 Animated:YES];
    }  
 
}

- (void)addToPhotoAlbums {
    
}

- (void)sendPicMail {
    
}

- (void)sendGifMail {
    
}

- (IBAction)play {
    if([timer isValid]) {
        [playButton setBackgroundImage:[UIImage imageNamed:@"playButton"] forState:UIControlStateNormal];
        [timer invalidate];
        timer = nil;
    }
    else {
        [playButton setBackgroundImage:[UIImage imageNamed:@"pauseButton"] forState:UIControlStateNormal];
        timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(playFrame) userInfo:nil repeats:YES];        
    }
}

- (void)stopPlay {
    if([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }    
}

- (void)playFrame {
    [self jumpToCell:selectedCellIndex + 1 Animated:NO];
    
    NSInteger frameNum = [((CellView *)[cells objectAtIndex:hightLightIndexNum]).label.text integerValue];
    NSLog(@"%d", frameNum);
    [delegate didJumpToFrame:frameNum];
}
@end
