//
//  InfoView.h
//  DrawCool
//
//  Created by RakuTech on 11-3-7.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopView.h"
#import "PaletteView.h"

@protocol InfoViewDelegate <PopDelegate>
@required
- (void)willReturnToDrawview;
- (void)willPushToPreviewController;
- (void)pushToPhotoLib; 
@end

@class Center;
@interface InfoView : PopView <PaletteViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>{
	id<InfoViewDelegate>delegate;
    Center *center;
    
    UIView *plateTopView, *plateView, *bigDisk;
    
    UIImageView *smallDisk;
    
    UITextField *titleInputView, *creatorInputView;
}
@property (nonatomic, assign)id<InfoViewDelegate>delegate;
@property (nonatomic, retain)Center *center;
@property (nonatomic, retain)IBOutlet UIImageView *smallDisk;
@property (nonatomic, retain)IBOutlet UIView *plateTopView, *plateView, *bigDisk;
@property (nonatomic, retain)IBOutlet UITextField *titleInputView, *creatorInputView;

- (IBAction)backgroundImageButton;
- (IBAction)backgroundColorButton;

- (IBAction)tapToCloseTextInput;
@end
