//
//  InfoView.m
//  DrawCool
//
//  Created by RakuTech on 11-3-7.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InfoView.h"
#import "Center.h"

@implementation InfoView
@synthesize delegate;
@synthesize center;
@synthesize plateTopView, plateView, bigDisk, smallDisk;
@synthesize titleInputView, creatorInputView; 


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code.

    }
    return self; 
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/
- (void)saveData {
    
}

- (void)loadData {
    NSDictionary *workDic = [center currentWorkInfo];
    
    titleInputView.text = [workDic objectForKey:@"title"];
    creatorInputView.text = [workDic objectForKey:@"creator"];
}

- (void)dealloc {
    [center release];
    [plateTopView release];
    [plateView release];
    [titleInputView release];
    [creatorInputView release];
    [super dealloc];
}

- (void)addGestureRecognizerTo:(UIView *)view
{    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    [panGesture setMaximumNumberOfTouches:1];
    [panGesture setDelegate:self];
    [view addGestureRecognizer:panGesture];
    [panGesture release];
}

- (void)panView:(UIPanGestureRecognizer *)gestureRecognizer {
    
    UIView *view = [gestureRecognizer view];

    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gestureRecognizer translationInView:[view superview]];
        
        if (view == plateTopView) {
            if(translation.x >= 0.0 && translation.y >= 20.0){
                [gestureRecognizer removeTarget:self action:@selector(panView:)];
                [self actPlate];
            }
        }
        else if(view == bigDisk) {
            if(translation.x >= 20.0 && translation.y >= 0.0){
                [gestureRecognizer removeTarget:self action:@selector(panView:)];
                [self actPlayer];
            }
        }
        
    }
}

- (IBAction)backgroundImageButton {
    [delegate pushToPhotoLib];
}

- (IBAction)backgroundColorButton {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubView" owner:self options:nil];
    PaletteView *paletteView = [nib objectAtIndex:1];
    [self addSubview:paletteView];
    paletteView.delegate = self;
    [paletteView setUseCover:YES];
    [paletteView loadData];
    [paletteView showAnimation:YES];
}

- (void)actPlayer {

    CAAnimationGroup *aniGroup1 = [self rotateQuarter:4 CycleTime:0.2];
    CAAnimationGroup *aniGroup2 = [self rotateQuarter:6 CycleTime:0.15];
    
    [bigDisk.layer addAnimation:aniGroup1 forKey:@"actionPlayer"];
    [bigDisk.layer setTransform:CATransform3DMakeRotation(4 * M_PI_2, 0.0, 0.0, 1.0)];
    [smallDisk.layer addAnimation:aniGroup2 forKey:nil];
    [smallDisk.layer setTransform:CATransform3DMakeRotation(6 * M_PI_2, 0.0, 0.0, 1.0)];
    
}

- (void)actPlate {
    
    CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"transform"];
    ani.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation((-1.0/6.0*M_PI), 0.0, 0.0, 1.0)];
    ani.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation((-1.0/12.0*M_PI), 0.0, 0.0, 1.0)];
    ani.duration = 0.15;
    ani.delegate = self;
    ani.removedOnCompletion = NO;

    [plateTopView.layer addAnimation:ani forKey:@"actionPlate"];
    [plateTopView.layer setTransform:CATransform3DMakeRotation((-1.0/12.0*M_PI), 0.0, 0.0, 1.0)];
}

- (void)showAnimation:(BOOL)animation {
    CGPoint oriPos = plateView.frame.origin;
    [plateTopView.layer setAnchorPoint:CGPointMake(0.0, 1.0)];
    [plateTopView.layer setPosition:oriPos];
    plateTopView.layer.transform = CATransform3DMakeRotation((-1.0/12.0*M_PI), 0.0, 0.0, 1.0);
    
    [plateView.layer setAnchorPoint:CGPointMake(0.0, 0.0)];
    [plateView.layer setPosition:oriPos];
    plateView.layer.transform = CATransform3DMakeRotation((0.0*M_PI), 0.0, 0.0, 1.0);
    
    [self addGestureRecognizerTo:plateTopView];
    [self addGestureRecognizerTo:bigDisk];
    
    if(animation){
        [self.layer setAnchorPoint:CGPointMake(0.05, 0.95)];
        self.frame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
        
        CABasicAnimation *ani0 = [self scale:CGPointMake(0.1, 0.1)To:CGPointMake(1.0, 1.0)];
        CABasicAnimation *ani1 = [self opacity:0.0 To:1.0];
        
        NSArray *array = [NSArray arrayWithObjects: ani0, ani1, nil];
        [self actionAnimation:array ForKey:@"show" Duration:0.5];
        
        self.alpha = 1.0;
    }
}

- (void)hideAnimation:(BOOL)animation {
    [delegate willReturnToDrawview];
    
    if(animation){
        CABasicAnimation *ani0 = [self scale:CGPointMake(1.0, 1.0) To:CGPointMake(0.1, 0.1)];
        CABasicAnimation *ani1 = [self opacity:1.0 To:0.0];        
        NSArray *array = [NSArray arrayWithObjects:ani0, ani1, nil];
        [self actionAnimation:array ForKey:@"hide" Duration:0.5];
        
        self.alpha = 0.0;
    }
    else {
        [self removeFromSuperview];
    }
}

- (void)animationDidStart:(CAAnimation *)anim {
    self.userInteractionEnabled = NO;
    [delegate setUserInteractionEnabled:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
    if(anim == [self.layer animationForKey:@"show"]){
        [self.layer removeAllAnimations];    
    }
    
    if(anim == [self.layer animationForKey:@"hide"]){
        [self.layer removeAllAnimations];
        [self removeFromSuperview];    
    }
    
    if(anim == [self.layer animationForKey:@"push"]){
        [self.layer removeAllAnimations];
        [self hideAnimation:NO];
        [delegate willPushToPreviewController];
    }
        
    if(anim == [plateTopView.layer animationForKey:@"actionPlate"]){
        [plateTopView.layer removeAllAnimations];
        [self hideAnimation:YES];
    }
    
    if(anim == [bigDisk.layer animationForKey:@"actionPlayer"]){
        [bigDisk.layer removeAllAnimations];
        [smallDisk.layer removeAllAnimations];

        CABasicAnimation *ani0 = [self scale:CGPointMake(1.0, 1.0) To:CGPointMake(0.1, 0.1)];
        CABasicAnimation *ani1 = [self opacity:1.0 To:0.0];
        
        NSArray *array = [NSArray arrayWithObjects:ani0, ani1, nil];
        [self actionAnimation:array ForKey:@"push" Duration:0.5];        
        self.alpha = 0.0;

    }
    
    self.userInteractionEnabled = YES;
    [delegate setUserInteractionEnabled:YES];
}

- (IBAction)tapToCloseTextInput {
    if([titleInputView isEditing]){
        [titleInputView endEditing:YES]; 
    }
    if([creatorInputView isEditing]){
        [creatorInputView endEditing:YES];
    }
    [[self viewWithTag:12] removeFromSuperview];
}

#pragma mark -
#pragma mark paletteViewDelegate

- (void)colorDidSet:(Color4)color {
    [EAGLContext setCurrentContext:center.context];
    [center initBackgroundWithColor:color];
    [center saveBackground];
}

- (Color4)returnColor {
    Color4 color;
    color.r = 0.5;
    color.r = 0.5;
    color.r = 0.5;
    color.r = 1.0;
    
    return color;
}
/*
- (void)setUserInteractionEnabled:(BOOL)set {
    self.userInteractionEnabled = set;
}
*/ 

#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UIButton *coverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    coverButton.tag = 12;
    [coverButton setFrame:self.frame];
    [coverButton setBackgroundColor:[UIColor clearColor]]; 
    [coverButton addTarget:self action:@selector(tapToCloseTextInput) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:coverButton];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [center saveChangedTitle:titleInputView.text Creator:creatorInputView.text];
}

@end
