//
//  TouchView.m
//  TouchMove
//
//  Created by RakuTech on 11-5-3.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "TouchView.h"

#define length(a)                   sqrt(pow(a.x, 2) + pow(a.y, 2))
#define distance(a, b)              sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2))
#define angle(a, b)                 acos((a.x * b.x + a.y * b.y)/(length(a)*length(b)))
#define midPoint(a, b)              CGPointMake(a.x - (a.x - b.x)/2, a.y - (a.y - b.y)/2)
#define transCoodForPoint(a)        CGPointMake(a.x, self.frame.size.height - a.y);
#define transCoodForVector(a)       CGPointMake(a.x, -a.y);


@implementation TouchView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        
        panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        [self addGestureRecognizer:panGesture];
        [panGesture release];
        [panGesture setDelegate:self];
        [panGesture setMinimumNumberOfTouches:2];
        [panGesture setMaximumNumberOfTouches:2];
        
        rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
        [self addGestureRecognizer:rotationGesture];
        [rotationGesture release];
        [rotationGesture setDelegate:self];
        
        pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
        [self addGestureRecognizer:pinchGesture];
        [pinchGesture release];
        [pinchGesture setDelegate:self];
        
        scale = 1.0;
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

}
*/
- (void)dealloc
{
    [super dealloc];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if([delegate isPreviewMode])
        return;
        
    if([touches count] < 2){
        tapCount++;
    }
    else {
        tapCount = tapCount + 2;
    }
    if(tapCount < 2){
        [delegate touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event]; 
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if([delegate isPreviewMode])
        return;
    
    if(tapCount < 2){
        [delegate touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];  
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if([delegate isPreviewMode])
        return;
    
    if([touches count] < 2){
        tapCount--;
    }
    else {
        tapCount = tapCount - 2;
    }
    if(tapCount < 2){
        [delegate touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event]; 
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    if([delegate isPreviewMode])
        return;
    
    if([touches count] < 2){
        tapCount--;
    }
    else {
        tapCount = tapCount - 2;
    }
    
    if(tapCount < 2){
        [delegate touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event];  
    }
}

- (void)rotation:(UIRotationGestureRecognizer *)gestureRecognizer {
        
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        rotation = [gestureRecognizer rotation];
        origin = transCoodForPoint([gestureRecognizer locationInView:self]);        
        [gestureRecognizer setRotation:0.0];
        
    }
    
}

- (void)pan:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        NSLog(@"pan");
        
        translation = transCoodForVector([gestureRecognizer translationInView:self]);
        [gestureRecognizer setTranslation:CGPointZero inView:self];
        
        [delegate transformOrigin:origin Translation:translation Angle:rotation Scaling:scale];
    }
}

- (void)scale:(UIPinchGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        NSLog(@"scale");
        
        scale = gestureRecognizer.scale;
        origin = transCoodForPoint([gestureRecognizer locationInView:self]); 
        [gestureRecognizer setScale:1.0];
        
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return ![delegate isPreviewMode];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    NSLog(@"shouldReceiveTouch");
    return ![delegate isPreviewMode];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
//    NSLog(@"ShouldBegin");
    return ![delegate isPreviewMode];
}

@end
